# requests模块深入使用

### 一、**添加代理**

> HTTP和HTTPS代理   

```python
import requests

# 访问公评网首页
url = 'http://17gp.com/'
proxy_dict = {
    "http": "http://host:port",
    "https": "https://host:port",
}
response = requests.get(url=url, proxies=proxy_dict)
```

> HTTP Basic Auth

```python
import requests

# 访问公评网首页
url = 'http://17gp.com/'
proxy_dict = {
    "http": "http://user:password@host:port",
    "https": "https://user:password@host:port",
}
response = requests.get(url=url, proxies=proxy_dict)
```

> SOCKS代理

```python
# 安装socks模块 pip3 install 'requests[socks]'

import requests

# 访问公评网首页
url = 'http://17gp.com/'
proxy_dict = {
    'http': 'socks5://user:password@host:port',
    'https': 'socks5://user:password@host:port',
}
response = requests.get(url=url, proxies=proxy_dict)
```

### 二、**超时设置**

> 基本设置

```python
import requests

# 访问公评网首页
url = 'http://17gp.com/'
# 设置超时1秒，如果1秒内没有响应，就会抛出异常。
time_out = 1
response = requests.get(url=url, timeout=time_out)
```

> 分阶段设置

```python
import requests

# 访问公评网首页
url = 'http://17gp.com/'
# 实际上，请求分为两个阶段，即连接（connect）和读取（read）,timeout将用作连接和读取这二者的timeout总和
# 如果要分别指定，就可以传入一个元组
time_out = (1, 1)
response = requests.get(url=url, timeout=time_out)
```

> 永久等待

```python
import requests

# 访问公评网首页
url = 'http://17gp.com/'
# 默认也是None，只要服务器还在运行且响应特别慢，会一直等待直到获取响应，不会返回超时错误
time_out = None
response = requests.get(url=url, timeout=time_out)
```

### 三、**retrying模块**

```python
# 安装retrying模块 pip3 install retrying

import requests
from retrying import retry

# 访问公评网首页
url = 'http://17gp.com/'

# 常用参数
# stop_max_attempt_number:设置最大重试次数
# wait_fixed:设置失败重试的间隔时间,单位毫秒
# stop_max_delay:设置失败重试的最大时间,单位毫秒,超出时间,则停止重试
@retry(stop_max_attempt_number=3)
def _parse_url(url):
    response = requests.get(url, timeout=3)  # 超时的时候回报错并重试
    return response

def parse_url(url):
    # 进行异常捕获
    try:
        response = _parse_url(url)
    except Exception as e:
        print(e)
        response = None
    return response
```

### 四、**SSL 证书验证**

> 警告页面， 即SSL 证书验证错误，没有被官方 CA 机构信任

![](../images/chapter1/p30.png)

> SSLError错误  

```python
requests.exceptions.SSLError: HTTPSConnectionPool(host='192.168.5.240', port=443): Max retries exceeded with url: /user/login (Caused by SSLError(SSLError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:852)'),))
```

> 设置verify参数  

```python
import requests

# 访问公司堡垒机登录页面
url = 'https://192.168.5.240/user/login'
# 设置verify参数为False，解决SSLError报错
response = requests.get(url=url, verify=False)
print(response.status_code)
```

> 返回状态码200，解决SSLError报错，但出现警告

```python
F:\environment\ershou_venv\lib\site-packages\urllib3\connectionpool.py:988: InsecureRequestWarning: Unverified HTTPS request is being made to host '192.168.5.240'. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning,
200
```

> 消除警告，该警告实际是建议我们指定证书

```python
import requests
from requests.packages import urllib3  # 可能出现报红，但不影响运行

# 设置忽略警告
urllib3.disable_warnings()
# 访问公司堡垒机登录页面
url = 'https://192.168.5.240/user/login'
# 设置verify参数为False，解决SSLError报错
response = requests.get(url=url, verify=False)
print(response.status_code)
```

> 返回结果

```python
200
```

### 五、**Cookies**

> 添加Cookies

```python
import requests

# 公评网首页
url = "http://17gp.com/"

# 方式一
cookies_str = 'gr_user_id=ea45e46f-7da7-4fb9-8061-9a0fbd96f374'  # cookies字符串
headers = {'Cookie': cookies_str}
response = requests.get(url=url, headers=headers)

# 方式二
cookies_dict = {'gr_user_id': 'ea45e46f-7da7-4fb9-8061-9a0fbd96f374'}  # cookies字典
response = requests.get(url=url, cookies=cookies_dict)
```

> 获取Cookies

```python
import requests
from requests.utils import dict_from_cookiejar

# 百度首页
url = "https://www.baidu.com/"
response = requests.get(url=url)

print(response.cookies)
print(type(response.cookies))

# 利用dict_from_cookiejar把cookiejar对象转化为字典
cookies_dict = dict_from_cookiejar(response.cookies)
print(cookies_dict)
```

>  返回结果，可成功得到 Cookies，可以发现它是 RequestCookieJar 类型，可以利用 dict_from_cookiejar 把 RequestCookieJar 对象转化为字典

```python
<RequestsCookieJar[<Cookie BDORZ=27315 for .baidu.com/>]>
<class 'requests.cookies.RequestsCookieJar'>
{'BDORZ': '27315'}
```

### 六、**会话保持**

> 不使用会话保持

```python
import requests

# 请求测试网站，并添加{'hifo':'1996'}的Cookie
requests.get('http://httpbin.org/cookies/set/hifo/1996')

# 请求测试网站，并添加{'HIFO':'2021'}的Cookie
requests.get('http://httpbin.org/cookies/set/HIFO/2021')

# 再请求测试网站，获取当前网站的 Cookies
response = requests.get('http://httpbin.org/cookies')

print(response.content.decode())
```

> 返回结果，Cookies为空

```python
{
  "cookies": {}
}
```

> 使用会话保持

```python
import requests

# 使用session会话
session = requests.Session()

# 请求测试网站，并添加{'hifo':'1996'}的Cookie
session.get('http://httpbin.org/cookies/set/hifo/1996')

# 请求测试网站，并添加{'HIFO':'2021'}的Cookie
session.get('http://httpbin.org/cookies/set/HIFO/2021')

# 再请求测试网站,获取当前网站的 Cookies
response = session.get('http://httpbin.org/cookies')

print(response.content.decode())
```

> 返回结果，拿到Cookies，且两次添加的Cookie都获取到

```python
{
  "cookies": {
    "HIFO": "2021", 
    "hifo": "1996"
  }
}

```