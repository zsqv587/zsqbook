# requests模块基本使用
### 一、requests与urllib

- `requests`的底层实现就是`urllib`
- `requests`在`Python2` 和`Python3`中通用，方法完全一样
- `requests`简单易用，对比`urllib.request.urlopen()`与`requests.get()` 

### 二、GET请求

```python
import requests

# 访问公评网首页
url = 'http://17gp.com/'
response = requests.get(url=url)
```

### 三、**response的常用属性**
- `response.text`
- `respones.content`
- `response.status_code`
- `response.request.headers`
- `response.headers`

### 四、**response.text 和response.content的区别**
- `response.text`
    - 类型：str
    - 解码类型： 根据HTTP 头部对响应的编码作出有根据的推测，推测的文本编码
    - 如何修改编码方式：`response.encoding='gb2312'`
- `response.content`
    - 类型：bytes
    - 解码类型： 没有指定
    - 如何修改编码方式：`response.content.deocde('gb2312')`
- 网页源码的通用解析方式
    - response.content.decode()
    - response.content.decode('gb2312')
    - response.text


### 五、**抓取二进制数据**

>代码示例：

```python
import requests

# 获取公评网logo
url = 'http://17gp.com/images/logo2.png'
response = requests.get(url=url)

print(response.text)
print('='*100)
print(response.content)
```
>返回结果：

```
# 仅为部分数据

'?z????]B???¤.óòù??Lj?—?”?yV?P3I?üf¤Qú?’??Qé^d×?è??è???)?éH”!¨?t?t??,8Pu?1?E??úZa)??N”±??J?3&	￡^*úQ¨O?1′^uYvüLJk?j???  ?ì??a?`    IEND?B`?'
====================================================================================================
b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x8f\x00\x00\x00-\x08\x06\x00\x00\x00\x97\xb78v\x00\x00\x00\tpHYs\x00\x00.#\x00\x00.#\x01x\xa5?v\x00\x00\nMiCCPPhotoshop ICC'     
```

### 六、**添加 headers**

```python
import requests

url = 'http://17gp.com/'
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
}
response = requests.get(url=url, headers=headers)
```

### 七、**添加请求参数**

```python
import requests

# 访问公评网二手房详情页
url = 'http://17gp.com/HouseResource/Details'
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
}
params = {'id': 'c2ab70290fb643c7be5b7de6c56d5462'}
response = requests.get(url=url, headers=headers, params=params)
```

### 八、**POST请求**

```python
import requests

# 登录公评网
url = 'http://17gp.com/Login/Check'
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
}
data = {'account': 12345678900, 'pwd': 123456, 'code': ''}
response = requests.post(url=url, headers=headers, data=data)
```

### 九、**上传文件**

```python
import requests

# 假设公评网提供上传接口
url = 'http://17gp.com/post'
# 需上传的文件
files = {'file': open('hifo_logo.png', 'rb')}
response = requests.post(url=url, files=files)
```
