# Json

### 一、Json的定义
&emsp;&emsp;JSON，全称为 JavaScript Object Notation, 也就是 JavaScript 对象标记，它通过对象和数组的组合来表示数据，构造简洁但是结构化程度非常高，是一种轻量级的数据交换格式。JSON便于阅读和编写，同时也方便了机器进行解析和生成。适用于进行数据交互的场景，比如网站前端与后台之间的数据交互。  
&emsp;&emsp;在 JavaScript 语言中，一切都是对象。因此，任何支持的类型都可以通过 JSON 来表示，例如字符串、数字、对象、数组等，但是对象和数组是比较特殊且常用的两种类型：  

-  对象：在 JavaScript 中是使用花括号 {&emsp;} 包裹起来的内容，数据结构为 {key1：value1, key2：value2, ...} 的键值对结构。在面向对象的语言中，key 为对象的属性，value 为对应的值。键名可以使用整数和字符串来表示。值的类型可以是任意类型。  
-  数组：数组在 JavaScript 中是方括号 [&emsp;] 包裹起来的内容，数据结构为 ["python", "java", "C", ...] 的索引结构。在 JavaScript 中，数组是一种比较特殊的数据类型，它也可以像对象那样使用键值对，但还是索引用得多。同样，值的类型可以是任意类型。  

### 二、Json的示例
以公评网首页【重庆市房价近一年走势图】为例：  

<img src="../images/chapter1/p19.png"  /> 

### 三、json模块
&emsp;&emsp;Python 提供了json模块来实现 JSON 文件的读写操作，json模块提供了四个功能：dumps、dump、loads、load，用于字符串 和 python数据类型间进行转换。
![](../images/chapter1/p20.png)   

-  **json.loads()**  

把Json格式字符串解码转换成Python对象，从json到python的类型转化对照如下：  

![](../images/chapter1/p21.png)   

- **json.dumps()**

实现python类型转化为json字符串，从python原始类型向json类型的转化对照如下：  

![](../images/chapter1/p22.png)   

- **json.load()、json.dump()**
	- json.load：读取文件中json形式的字符串元素 转化成python类型
	- json.dump：将Python内置类型序列化为json对象后写入文件
	- 类文件对象：具有read()或者write()方法的对象就是类文件对象，比如f = open(“a.txt”,”r”) f就是类文件对象

### 四、Json的使用
```python
import json

# json.dumps 实现python类型转化为json字符串
# indent实现换行和空格
# ensure_ascii=False实现让中文写入的时候保持为中文
json_str = json.dumps(hifo_dict,indent=2,ensure_ascii=False)

# json.loads 实现json字符串转化为python类型
hifo_dict = json.loads(json_str)

# json.dump 实现把python类型写入类文件对象
with open("hifo.txt","w") as f:
    json.dump(hifo_dict,f,ensure_ascii=False,indent=2)
    
# json.load 实现类文件对象中的json字符串转化为python类型
with open("hifo.txt","r") as f:
    hifo_dict = json.load(f)
```

### 五、JsonPath
&emsp;&emsp;JsonPath 是一种信息抽取类库，是从JSON文档中抽取指定信息的工具，提供多种语言实现版本，包括：Javascript、Python、PHP 和 Java。  
&emsp;&emsp;JsonPath 对于 JSON 来说，相当于 XPATH 对于 XML。

> 下载地址：https://pypi.python.org/pypi/jsonpath  
> 安装方法：点击Download URL链接下载jsonpath，解压之后执行python setup.py install  
> 官方文档：http://goessner.net/articles/JsonPath  

### 六、JsonPath与XPath语法对比
![](../images/chapter1/p23.png)   
