# RE
### 一、RE的概述

&emsp;&emsp;<strong>正则表达式</strong>，又称正规表示式、正规表示法、正规表达式、规则表达式、常规表示法（全称：<strong>Regular Expression</strong>，在代码中常简写为regex、regexp或RE），是计算机科学的一个概念。正则表达式使用单个字符串来描述、匹配一系列匹配某个句法规则的字符串。在很多文本编辑器里，正则表达式通常被用来检索、替换那些匹配某个模式的文本。   
&emsp;&emsp;Regular Expression的“Regular”一般被译为“正则”、“正规”、“常规”。此处的“Regular”即是“规则”、“规律”的意思，Regular Expression即“描述某种规则的表达式”之意。  

###  二、常用的匹配规则
- **表示字符**

>![](../images/chapter1/p24.png)  

- **表示数量**

>![](../images/chapter1/p25.png)  

- **表示边界**

>![](../images/chapter1/p26.png)  

- **匹配分组**

>![](../images/chapter1/p27.png)  

### 三、re模块
- **match()**：该方法会尝试从字符串的起始位置匹配正则表达式，如果匹配，就返回匹配成功的结果；如果不匹配，就返回 None。

>代码示例：  

```python
import re
#如果hello的首字符小写，那么正则表达式需要小写的h  
ret = re.match("h", "hello Hifo123")
print('ret_1:', ret.group())

#如果hello的首字符大写，那么正则表达式需要大写的H
ret = re.match("H", "Hello Hifo123")
print('ret_2:', ret.group())

#大小写h都可以的情况
ret = re.match("[hH]", "hello Hifo123")
print('ret_3:', ret.group())
ret = re.match("[hH]", "Hello Hifo123")
print('ret_4:', ret.group())

#匹配0到9第一种写法
ret = re.match("[0123456789]", "123 Hello Hifo123")
print('ret_5:', ret.group())

#匹配0到9第二种写法
ret = re.match("[0-9]", "123 Hello Hifo123")
print('ret_6:', ret.group())
```

>返回结果：  

```python
ret_1: h
ret_2: H
ret_3: h
ret_4: H
ret_5: 1
ret_6: 1
```


- **search()**：与match()不同，该方法在匹配时会扫描整个字符串，然后返回第一个成功匹配的结果。也就是说，正则表达式可以是字符串的一部分，在匹配时，search() 方法会依次扫描字符串，直到找到第一个符合规则的字符串，然后返回匹配内容，如果搜索完了还没有找到，就返回 None。

>代码示例：  

```python
import re

ret = re.search(r"\d+", "汇丰智力产业组织成立于1996年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼, 办公总面积2000余平方米, 是重庆成立早、规模大、资质等级高和人员多的集成化综合评估、咨询机构之一。")
print(ret.group())
```

>返回结果：  

```python
1996
```

- **findall()**：该方法会搜索整个字符串，然后返回匹配正则表达式的所有内容，为list类型。

>代码示例：  

```python
import re

ret = re.findall(r"\d+", "汇丰智力产业组织成立于1996年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼, 办公总面积2000余平方米, 是重庆成立早、规模大、资质等级高和人员多的集成化综合评估、咨询机构之一。")
print(ret)
```

>返回结果：  

```python
['1996', '38', '30', '2000']
```

- **sub()**：该方法可以将将匹配到的数据进行替换。

>代码示例：  

```python
import re

ret = re.sub(r"\d+", "2021", "汇丰智力产业组织成立于1996年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼")
print('ret_1:', ret)

#设置count参数
ret = re.sub(r"\d+", "2021", "汇丰智力产业组织成立于1996年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼", count=1)
print('ret_2:', ret)
```

>返回结果：  

```python
ret_1: 汇丰智力产业组织成立于2021年, 组织总部位于重庆市渝中区解放碑青年路2021号国贸中心A幢2021楼
ret_2: 汇丰智力产业组织成立于2021年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼
```

- **compile()**：可以将正则字符串编译成正则表达式对象，以便在后面的匹配中复用。

>代码示例：  

```python
import re

pattern = re.compile('\d+')

ret = pattern.sub("2021", "汇丰智力产业组织成立于1996年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼", count=1)
print(ret)
```

>返回结果：  

```python
汇丰智力产业组织成立于2021年, 组织总部位于重庆市渝中区解放碑青年路38号国贸中心A幢30楼
```

>此外，compile() 还可以传入修饰符，例如 re.S 等修饰符，这样在 search()、findall() 等方法中就不需要额外传了。所以，compile() 方法可以说是给正则表达式做了一层封装，以便更好地复用。