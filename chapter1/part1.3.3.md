# Xpath
### 一、Xpath与lxml
- <strong>XPath </strong>(XML Path Language) 是一门在 HTML\XML 文档中查找信息的语言，可用来在 HTML\XML 文档中对元素和属性进行遍历。
- <strong>lxml</strong>是一款高性能的 Python HTML/XML 解析器，可以在Python中利用XPath，来快速的定位特定元素以及获取节点信息

### 二、XPath开发工具
- 开源的XPath表达式编辑工具:XMLQuire(XML格式文件可用)
- Chrome插件 XPath Helper  
- Firefox插件 XPath Checker

### 三、XPath常用规则
![](../images/chapter1/p28.png)  

### 四、XPath运算符
![](../images/chapter1/p29.png)

### 五、lxml使用
>代码示例：

```python
# 使用 lxml 的 etree 库
from lxml import etree 

html_str = '''
<div>
    <ul>
         <li class="item-0"><a href="hifo.html">first item</a></li>
         <li class="item-1"><a href="hifo1.html">second item</a></li>
         # 注意，此处缺少一个 </li> 闭合标签
         <li class="item-1"><a href="hifo2.html">third item</a>
     </ul>
 </div>
'''

# 利用etree.HTML，将字符串解析为HTML文档
html = etree.HTML(html_str) 

# 按字符串序列化HTML文档
result = etree.tostring(html) 

print(result)
```

>返回结果：

```python
<html><body>
<div>
    <ul>
         <li class="item-0"><a href="hifo.html">first item</a></li>
         <li class="item-1"><a href="hifo1.html">second item</a></li>
         <li class="item-1"><a href="hifo2.html">third item</a></li>
	</ul>
</div>
</body></html>
```

>lxml 可以自动修正 html 代码，例子里不仅补全了 li 标签，还添加了 body，html 标签。

### 六、相关链接
- [W3School官方文档](http://www.w3school.com.cn/xml/index.asp)
- [菜鸟教程](http://www.w3school.com.cn/xml/index.asp)
- [lxml下载](https://pypi.org/project/lxml/3.4.4/)