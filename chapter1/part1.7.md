# Scrapy框架

### 一、Scrapy的作用

&emsp;&emsp;基于 requests 模块，再配合其它一些工具引擎（比如：puppeteer、selenium等）基本可以解决 <font  color=red>90%</font> 的爬虫问题，那剩下的 <font  color=red>10%</font> 可以用 <strong>Scrapy</strong> 解决吗？  

![](../images/chapter1/p31.png)

&emsp;&emsp;Scrapy是一个为了爬取网站数据，提取结构性数据而编写的应用框架，只需要实现少量的代码，就能够快速的抓取，便于程序维护迭代。Scrapy 使用了Twisted异步网络框架，可以加快爬取速度。

### 二、爬虫流程
- **爬虫流程**
>![](../images/chapter1/p32.png)  

- **改写后的爬虫流程**
>![](../images/chapter1/p33.png)

- **Scrapy爬虫流程**
>![](../images/chapter1/p34.png)

- **Scrapy爬虫流程概述**
	1. 调度器把requests—>引擎—>下载中间件—>下载器；
	2. 下载器发送请求—>获取响应—>下载中间件—>引擎—>爬虫中间件—>爬虫；
	3. 爬虫提取url地址，构造成request对象—>爬虫中间件—>引擎—>调度器；
	4. 爬虫提取数据—>引擎—>管道；
	5. 管道进行数据的处理和保存。

###  三、Scrapy各模块作用

![](../images/chapter1/p35.png)

### 四、Scrapy相关链接
- [Scrapy框架官方网址](https://docs.scrapy.org/en/latest/)
- [Scrapy中文维护站点](http://scrapy-chs.readthedocs.io/zh_CN/latest/index.html)