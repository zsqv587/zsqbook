# 爬虫基础
### 一、HTTP和HTTPS
- **URI、URL、URN**
	- URI：Uniform Resource Identifier，即统一资源标志符；  
	- URL：Universal Resource Locator，即统一资源定位符；   
	- URN：Universal Resource Name，即统一资源名称。  
  

&emsp;&emsp;举例来说，http://17gp.com/favicon.ico 是 公评网 的网站图标链接，它是一个 URL，也是一个 URI。即有这样的一个图标资源，我们用 URL/URI 来唯一指定了它的访问方式，这其中包括了访问协议 http、访问路径（/ 即根目录）和资源名称 favicon.ico。通过这样一个链接，我们便可以从互联网上找到这个资源，这就是 URL/URI。  
&emsp;&emsp;URL 是 URI 的子集，也就是说每个 URL 都是 URI，但不是每个 URI 都是 URL，因为URI 还包括一个子类叫作 URN，URN 只命名资源而不指定如何定位资源。但是在目前的互联网中，URN 用得非常少，所以几乎所有的 URI 都是 URL，一般的网页链接我们既可以称为 URL，也可以称为 URI，通常爬虫中都习惯称为 URL。  
![](../images/chapter1/p11.png)  

- **超文本：其英文名称叫作 hypertext。**  

&emsp;&emsp;通常在浏览器里看到的网页就是超文本解析而成的，其网页源代码是一系列 HTML 代码，里面包含了一系列标签，比如 img 显示图片，p 指定显示段落等。浏览器解析这些标签后，便形成了我们平常看到的网页，而网页的源代码 HTML 就可以称作超文本。  
&emsp;&emsp;例如，在 Chrome 浏览器里面打开任意一个页面，如 公评网 首页，右击任一地方并选择 “检查” 项（或者直接按快捷键 F12），即可打开浏览器的开发者工具，这时在 Elements 选项卡即可看到当前网页的源代码，这些源代码都是超文本，如下图所示。  

![](../images/chapter1/p12.png)   

- **HTTP和HTTPS**  

&emsp;&emsp;HTTP （默认端口80）的全称是 Hyper Text Transfer Protocol，中文名叫作超文本传输协议。HTTP 协议是用于从网络传输超文本数据到本地浏览器的传送协议，它能保证高效而准确地传送超文本文档。HTTP 由万维网协会（World Wide Web Consortium）和 Internet 工作小组 IETF（Internet Engineering Task Force）共同合作制定的规范，目前广泛使用的是 HTTP 1.1 版本。  
&emsp;&emsp;HTTPS （默认端口443）的全称是 Hyper Text Transfer Protocol over Secure Socket Layer，是以安全为目标的 HTTP 通道，简单讲是 HTTP 的安全版，即 HTTP 下加入 SSL 层，简称为 HTTPS。HTTPS 的安全基础是 SSL，因此通过它传输的内容都是经过 SSL 加密的，它的主要作用可以分为两种：①建立一个信息安全通道来保证数据传输的安全；②确认网站的真实性，凡是使用了 HTTPS 的网站，都可以通过点击浏览器地址栏的锁头标志来查看网站认证之后的真实信息，也可以通过 CA 机构颁发的安全签章来查询。  

- **HTTP 请求过程**  

![](../images/chapter1/p13.png)   

### 二、Request和Response
##### Request 
&emsp;&emsp;由客户端向服务端发出，可以分为 4 部分内容：请求方法（Request Method）、请求的网址（Request URL）、请求头（Request Headers）、请求体（Request Body）。  

- **Request Method**
	- 常见的请求方法  
	
	![](../images/chapter1/p15.png)  

	- 其他 HTTP 请求方法  

	![](../images/chapter1/p16.png)  

- **Request URL**

  请求的网址，即统一资源定位符 URL，它可以唯一确定我们想请求的资源。

- **Request Headers**
  - Host (主机和端口号)
  - Connection (链接类型)
  - Upgrade-Insecure-Requests (升级为HTTPS请求)
  - <font face="微软雅黑"  color=red>User-Agent</font> (简称 UA，它是一个特殊的字符串头，可以使服务器识别客户使用的操作系统及版本、浏览器及版本等信息。在做爬虫时加上此信息，可以伪装为浏览器；如果不加，很可能会被识别出为爬虫)
  - Accept (传输文件类型)
  - <font face="微软雅黑"  color=red>Referer</font> (用来标识这个请求是从哪个页面发过来的，服务器可以拿到这一信息并做相应的处理，如作来源统计、防盗链处理等)
  - Accept-Encoding（文件编解码格式）
  - <font face="微软雅黑"  color=red>Cookie</font> （也常用复数形式 Cookies，这是网站为了辨别用户进行会话跟踪而存储在用户本地的数据。它的主要功能是维持当前访问会话。例如，我们输入用户名和密码成功登录某个网站后，服务器会用会话保存登录状态信息，后面我们每次刷新或请求该站点的其他页面时，会发现都是登录状态，这就是 Cookies 的功劳。Cookies 里有信息标识了我们所对应的服务器的会话，每次浏览器在请求该站点的页面时，都会在请求头中加上 Cookies 并将其发送给服务器，服务器通过 Cookies 识别出是我们自己，并且查出当前状态是登录状态，所以返回结果就是登录之后才能看到的网页内容）
  - x-requested-with :XMLHttpRequest (是Ajax 异步请求)
  - Content-Type（也叫互联网媒体类型（Internet Media Type）或者 MIME 类型，在 HTTP 协议消息头中，它用来表示具体请求中的媒体类型信息。例如，text/html 代表 HTML 格式，image/gif 代表 GIF 图片，application/json 代表 JSON 类型）

- **Request Body**

  请求体一般承载的内容是 POST 请求中的表单数据，而对于 GET 请求，请求体则为空。  

  ![](../images/chapter1/p17.png)  

##### Response
&emsp;&emsp;由服务端返回给客户端，可以分为三部分：响应状态码（Response Status Code）、响应头（Response Headers）和响应体（Response Body）。  

- **Response Status Code**

	![](../images/chapter1/p18.png)   

- **Response Headers**
	- Date（标识响应产生的时间）
	- Last-Modified（指定资源的最后修改时间）
	- Content-Encoding（指定响应内容的编码）
	- Content-Type（文档类型，指定返回的数据类型是什么，如 text/html 代表返回 HTML 文档，application/x-javascript 则代表返回 JavaScript 文件，image/jpeg 则代表返回图片）
	- <font face="微软雅黑"  color=red>Set-Cookie</font>（设置 Cookies。响应头中的 Set-Cookie 告诉浏览器需要将此内容放在 Cookies 中，下次请求携带 Cookies 请求）
	- Expires（指定响应的过期时间，可以使代理服务器或浏览器将加载的内容更新到缓存中）

- **Response Body**

&emsp;&emsp;响应体是服务器返回给客户端的文本信息，而爬取数据时主要也是通过响应体得到网页的源代码、JSON 数据等，然后从中做相应内容的提取。  
