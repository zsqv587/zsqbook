# 铜仁市新房爬虫

### 一、项目目标
1. 爬取网站：[铜仁市房地产交易管理处](http://www.trfcj.com/)
2. 爬取目标：完成**楼盘 + 楼栋 + 房号**详细信息的爬取、入库、更新

### 二、创建项目

```python
# 配置好python虚拟环境
# win+R输入cmd打开黑窗口  

# 创建项目
# 如已有项目则可跳过此步骤
scrapy startproject FDC_spider
```

### 三、创建爬虫

```python
# 切换到项目目录
cd FDC_spider
# 创建爬虫
scrapy genspider tongren trfcj.com
```

### 四、定位爬虫入口

![](../images/chapter2/铜仁市/p49.png)

### 五、项目列表首页

![](../images/chapter2/铜仁市/p50.png)

进入项目列表页，抓包分析后不难发现项目列表信息为`ajax`异步加载。如上图所示，红色方框为网页项目列表信息，红色圆框为`ajax`请求`url`，蓝色圆框为字段对应信息。

```json
{
	"Records": [{
		"IsChecked": false,
		"UpdateColumns": null,
		"RecordId": 1,
		"ProjectId": "5206000000000362",
		"ProjectName": "山水云天三期",
		"MLandAgentCode": "52060000000141",
		"MLandAgentName": "贵州省铜仁市鸿兴房地产开发有限公司",
		"PrePressionCertNo": null,
		"PrePressionNumber": null,
		"TotalNumber": null,
		"PrePressionArea": null,
		"PlanArea": 150000.00,
		"ProjectFileNum": "2019-520602-70-03-412905",
		"LandNumber": "碧江国用(2014)第2617号",
		"ProjectAddress": "铜仁市碧江区清水塘小学北侧"
	}, {
		"IsChecked": false,
		"UpdateColumns": null,
		"RecordId": 2,
		"ProjectId": "5206000000000361",
		"ProjectName": "碧江区2018年河西片区（城中村）城市棚户区改造项目（二期）",
		"MLandAgentCode": "52060000000103",
		"MLandAgentName": "铜仁市碧江城市建设开发投资有限公司",
		"PrePressionCertNo": "2021010",
		"PrePressionNumber": null,
		"TotalNumber": 576,
		"PrePressionArea": null,
		"PlanArea": 272069.00,
		"ProjectFileNum": "碧发改投资（2018）33号",
		"LandNumber": "黔（2020）铜仁市不动产权第0005481号",
		"ProjectAddress": "碧江区市中街道桂花塘片区（原水务局）"
	}...],
	"CurPage": 1,
	"PageSize": 20,
	"TotalPage": 18,
	"TotalRecords": 354,
	"CurPageRecords": 0
}
```

解析列表页响应信息，为`Json`字符串。其中包含了项目的部分字段数据、当前的页数、每页的项目数（20/页）、总页数、项目总数等信息。获取项目列表首页信息核心代码如下：

```python
class TongrenSpider(scrapy.Spider):
    name = 'tongren'
    allowed_domains = ['trfcj.com']
    start_urls = ['http://www.trfcj.com/Client/Nanjiang/Second/Second_HouseManger.aspx?RelationCID=&PageSize=5']
    # GET 项目列表url
    project_li_url_temp = 'http://www.trfcj.com/Client/Nanjiang/Scripts/Paging/PagingHandler.ashx?MLandAgentName=&ProjectName=&ProjectAddress=&PrePressionCertNo=&&act=Project&columnID=&curPage={}&pageSize=20'

    def start_requests(self):
        """
        重构项目列表页首页请求
        :return:
        """
        headers: dict = {
            'Referer': self.start_urls[0],
        }
        page_num: int = 1
        yield scrapy.Request(
            self.project_li_url_temp.format(page_num, ),
            headers=headers,
            meta=dict(page_num=page_num, ),
        )

    def parse(self, response):
        """
        项目列表页
        :param response:
        :return:
        """
        page_num = copy(response.meta['page_num'])
        try:
            resp_dict: dict = json.loads(response.body.decode())
            # 提取总页数
            total_page_num: int = int(resp_dict.get('TotalPage', None))
            # 提取当前页项目列表信息
            records_li: list = resp_dict.get('Records', None)
            assert records_li, f'第{page_num}页  项目列表获取为空'
        except AssertionError as e:
            logger.error(e)
        except Exception as e:
            logger.error(f'第{page_num}页  项目列表获取出错，msg:{e if e else "error"}')
        else:
            # 遍历，获取项目信息
            for project_dict in records_li:
                item_eg = FdcEstateGuidItem()
                item_eg['projectName'] = project_dict.get('ProjectName', None)  # 项目名称
                item_eg['projectId'] = project_dict.get('ProjectId', None)  # 项目id
                '''其余代码已省略'''
```

### 六、项目列表翻页

![](../images/chapter2/铜仁市/p51.png)

进行项目列表页翻页抓包调试，解析后发现同样为`ajax`请求，请求方式为`GET`，请求`url`与列表首页的`url`基本一致，其中`curPage`为当前页数、`pageSize`为每页项目数（可以固定为20）、`rnd`为0～1之间的随机小数（经测试去掉该参数也可以正常获取数据 或者 用`random`模块实现）。项目列表翻页核心代码如下：

```python
def parse(self, response):
    """
    项目列表页
    :param response:
    :return:
    """
    page_num = copy(response.meta['page_num'])
    try:
       '''其余代码已省略'''
    except Exception as e:
       '''其余代码已省略'''
    else:
        #  翻页
        if page_num < total_page_num:
            headers: dict = {
                'Referer': self.start_urls[0],
            }
            page_num += 1
            yield scrapy.Request(
                self.project_li_url_temp.format(page_num, ),
                headers=headers,
                meta=dict(page_num=page_num, ),
                priority=10,
            )
```

### 七、项目详情

![](../images/chapter2/铜仁市/p52.png)

项目详情数据主要包括**黑色圆框**中的开发商详情和**蓝色圆框**中的项目详情，先点击进入项目详情页面如下图：

![](../images/chapter2/铜仁市/p53.png)

经抓包解析，不难发现为`GET`请求，响应为`html`字符串，请求`url`中参数只有`id`在变化，且它的值就是该项目的`projectId`值，该值在前面的项目列表中就已经获取到。项目详情页的核心代码如下：

```python
def parse(self, response):
    """
    项目列表页
    :param response:
    :return:
    """

    '''其余代码已省略'''
    
    else:
        # 遍历，获取项目信息
        for project_dict in records_li:
            # 获取项目详情
            if item_eg['projectId']:
                headers: dict = {
                    'Referer': self.start_urls[0],
                }
                yield scrapy.Request(
                    self.project_url_temp.format(item_eg['projectId']),
                    headers=headers,
                    callback=self.parse_project_detail,
                    meta=dict(item_eg=deepcopy(item_eg), company_id=deepcopy(company_id), ),
                    priority=20,
                )
            else:
                logger.error('第{}页-{} 项目id提取为空，无法访问项目详情'.format(page_num, item_eg['projectName']))
    
def parse_project_detail(self, response):
    """
    获取项目详情
    :param response:
    :return:
    """
    item_eg = copy(response.meta['item_eg'])
    company_id = copy(response.meta['company_id'])
    # 提取项目详情信息
    item_eg['projectUrl'] = response.request.url  # 项目url
    item_eg['projectNature'] = response.xpath(
        '//td[contains(text(),"主体性质")]/following-sibling::td[1]/text()').extract_first()  # 项目性质
    item_eg['planningCommencementDate'] = response.xpath(
        '//td[contains(text(),"计划开工日期")]/following-sibling::td[1]/text()').extract_first()  # 计划开工日期
    
    '''其余代码已省略'''
```

![](../images/chapter2/铜仁市/p54.png)

同理，开发商详情与项目详情基本类似，为`GET`请求，响应为`html`字符串，请求`url`中参数也只有`id`在变化，其值同样就是该项目的`projectId`值。开发商详情页的核心代码如下：

```python
def parse_project_detail(self, response):
    """
    获取项目详情
    :param response:
    :return:
    """
  	
    '''其余代码已省略'''

    # 获取开发商详情
    if company_id:
        headers: dict = {
            'Referer': self.start_urls[0],
        }
        yield scrapy.Request(
            self.company_url_temp.format(company_id),
            headers=headers,
            callback=self.parse_company_detail,
            meta=dict(item_eg=deepcopy(item_eg), ),
            priority=40,
            dont_filter=True,
        )
    else:
        logger.warning('{} 开发商id提取为空，无法获取开发商详情信息'.format(item_eg['projectName']))
        item_eg['qualificationGrade'] = None  # 资质等级
        item_eg['qualificationCertificateNo'] = None  # 资质证书编号
        yield item_eg

def parse_company_detail(self, response):
    """
    获取开发商详情
    :param response:
    :return:
    """
    item_eg = copy(response.meta['item_eg'])
    item_eg['qualificationGrade'] = response.xpath(
        "//td[contains(text(),'资质等级')]/following-sibling::td[1]/text()").extract_first()  # 资质等级
    item_eg['qualificationCertificateNo'] = response.xpath(
        "//td[contains(text(),'资质证编号')]/following-sibling::td[1]/text()").extract_first()  # 资质证书编号
    yield item_eg
```

### 八、楼栋列表

![](../images/chapter2/铜仁市/p55.png)

经页面解析，该网站**楼栋列表信息与项目详情信息在同一个页面**中（注意：部分项目没有楼栋列表信息，需做日志处理），即不需要重新构造、发起请求，且**没有楼栋详情页信息**，而楼栋的总楼层和单元数则可在房号获取时统计后得到。楼栋列表核心代码如下：

```python
def parse_project_detail(self, response):
    """
    获取项目详情
    :param response:
    :return:
    """
    
    '''其余代码已省略'''
    
    #  获取楼栋列表分组
    tr_li = response.xpath("//table[@id='table1']//tbody/tr")
    if tr_li:
        # 遍历提取楼栋信息
        for tr_html_obj in tr_li:
            item_bd = FdcBuildingItem()
            item_bd['projectName'] = item_eg['projectName']  # 项目名称
            item_bd['projectId'] = item_eg['projectId']  # 项目id
            item_bd['districtName'] = item_eg['districtName']  # 项目区域/行政区划
            item_bd['buildingUrl'] = tr_html_obj.xpath('./td[1]/a/@href').extract_first()  # 楼栋url
            item_bd['buildingId'] = self.get_building_id(item_bd['buildingUrl'])  # 楼栋id
            item_bd['blockName'] = tr_html_obj.xpath('./td[2]/text()').extract_first()  # 楼栋名字
            item_bd['approvalRoomNo'] = tr_html_obj.xpath('./td[3]/text()').extract_first()  # 批准套数
            item_bd['approvalArea'] = tr_html_obj.xpath('./td[4]/text()').extract_first()  # 批准面积
            item_bd['residenceAvgPrice'] = tr_html_obj.xpath('./td[5]/text()').extract_first()  # 住宅均价
            item_bd['nonResidenceAvgPrice'] = tr_html_obj.xpath('./td[6]/text()').extract_first()  # 非住宅均价
```

### 九、房号列表

![](../images/chapter2/铜仁市/p56.png)

点击楼栋列表中的**查看楼盘**即可进入相应楼栋的房号列表页面，抓包发现，该请求为`GET`请求，请求`url`从`a`标签超链接中的`href`属性中获取即可，响应为`html`字符串。获取房号列表核心代码如下：

```python
def parse_project_detail(self, response):
    """
    获取项目详情
    :param response:
    :return:
    """
    
    '''其余代码已省略'''
    
    #  获取楼栋列表分组
    tr_li = response.xpath("//table[@id='table1']//tbody/tr")
    if tr_li:
        # 遍历提取楼栋信息
        for tr_html_obj in tr_li:
            # 获取房号列表
            if item_bd['buildingUrl']:
                yield scrapy.Request(
                    item_bd['buildingUrl'],
                    callback=self.parse_room_li,
                    meta=dict(item_bd=deepcopy(item_bd), ),
                    priority=30,
                )
            else:
                logger.error('{}-{} 楼栋url提取为空，url：{}'.format(item_bd['projectName'], item_bd['blockName'],
                                                             response.request.url))
```

### 十、房号详情之验证码

![](../images/chapter2/铜仁市/p57.png)

点击房号列表页面的任意房号，跳转房号详情页面之前会出现验证码（暂时先不管），正确输入验证码后，即进入房号详情页面，如下图所示：

![](../images/chapter2/铜仁市/p58.png)

整个请求是`ajax`请求，请求方式是`POST`，请求`url`由`js`动态生成，详细的请求体参数如下图所示：

![](../images/chapter2/铜仁市/p59.png)

分析请求体参数，其中**蓝色方框**中就是前面输入的验证码信息`241H`，而**红色方框**（即整个`Form data`）中就包含了房号详情的数据。因此可以大胆猜测**房号详情信息并不需要访问详情页面，而是在发起详情页请求前就能获取到**，不需要发起详情页请求，即不需要处理验证码问题。为了证明猜测的正确性，需要进行`js`抓包分析。

### 十一、房号详情之 `js`抓包

![](../images/chapter2/铜仁市/p60.png)

整个抓包过程比较简单，首先可以看到每个房号的`div`标签中都绑定了`ondbClick`函数，其参数就是`div`标签的`id`值（也是房号的`id` ），因此不难获取到`ondbClick`函数的代码如下：

```javascript
function ondbClick(obj) {
    $("#verification").css("display", "block");
    $("#sh_vcode").val("");
    $("#houseTitle").css("display", "none");
    $("#divTable").css("display", "none");
    curHouseId = obj;
    displayPng();
    fnUpdateCode(root, document.getElementById("imgCode"));
    $("#dialog").dialog({
        height: 460,
        width: 900,
        modal: true
    });
}

function displayPng() {
    for (var i = 0; i < document.images.length; i++) {
        var img = document.images[i]
        var imgName = img.src.toUpperCase()
        if (imgName.substring(imgName.length - 3, imgName.length) == "PNG") {
            var imgID = (img.id) ? "id='" + img.id + "' " : ""
            var imgClass = (img.className) ? "class='" + img.className + "' " : ""
            var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
            var imgStyle = "display:inline-block;" + img.style.cssText
            if (img.align == "left") imgStyle = "float:left;" + imgStyle
            if (img.align == "right") imgStyle = "float:right;" + imgStyle
            if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle
            var strNewHTML = "<span " + imgID + imgClass + imgTitle
           + " style=\"" + "width:" + img.width + "px;height:" + img.height + "px;" + imgStyle + ";"
           + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
           + "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>"
            img.outerHTML = strNewHTML
            i = i - 1
        }
    }
}

function fnUpdateCode(root, obj) {//验证码跟换
    if (obj) {
        obj.src = root + "ValidateCode/ValidateCode.aspx?code=" + new Date().toString();
    }
}
```

分析 `ondbClick`函数，该函数主要是通过`id`选择器对部分标签的属性值进行修改，并先后调用了`displayPng`函数和`fnUpdateCode`函数（其代码也在上面）。经大致分析，`displayPng`函数主要是实现了验证码页面的代码生成，`fnUpdateCode`函数则是构造了验证码图片的`src`值。因此，通过调用`ondbClick`函数，实现的功能只是展示出验证码页面，并无房号详情的相关逻辑。接着再对验证码页面进行抓包。

![](../images/chapter2/铜仁市/p61.png)

进入验证码页面，不难发现**确定**按钮绑定了`onSubmit`函数，获取`onSubmit`函数详细代码如下:

```javascript
function onSubmit() {
            var flag = true;
            var count = 0;
            $("[id^=sh_]").each(function () {
                var curObj = $(this);
                var key = curObj.attr("id").split("_")[1];
                count += checkData(curObj);
            });
            if (count > 0) {
                flag = false;
            }
            if (flag) {
                var house = houselist.first({ HouseId: curHouseId });
                house["VCode"] = $("#sh_vcode").val();
                $.ajax({
                    url: "HouseDetail.ashx?rnd=" + new Date(),
                    type: "Post",
                    data: $.toJSON(house),
                    success: function (msg) {
                        if (msg == "false") {
                            $("#sh_vcode").css({ "border-color": "red", "color": "red" }).val("验证码输入错误！").attr("isErr", "true");
                        }
                        else {
                            var html = msg.split("|");
                            $("#verification").css("display", "none");
                            $("#houseTitle").css("display", "block");
                            $("#divTable").css("display", "block");
                            $("#HouseIds").html(html[0]);
                            $("#divTable").empty();
                            $("#divTable").append(html[1]);
                            $("#divTable").append(html[2]);
                            if ($("#houseState").hasClass("classBorder")) {
                                $("#houseMsg").addClass("classBorder");
                                $("#houseState").removeClass("classBorder");
                            }

                        }
                    }

                });
            }
        }


function checkData(curObj) {
    var result = 0;
    if (curObj.attr("isEmpty") == "true") {
        result = 1;
    }
    else if (curObj.attr("isErr") == "true") {
        result = 2;
    }
    if (result == 0) {
        result = verify(curObj);
        if (result != 0) {
            var key = curObj.attr("id").split("_")[1];
            var msg = "";
            if (result == 1) {
                msg = curObj.attr("msg") + "不能为空！";
                curObj.attr("isEmpty", "true");
            }
            else if (result == 2) {
                var curVal = curObj.val();
                msg = "请输入正确的" + curObj.attr("msg") + "！";
                curObj.attr("isErr", "true");
            }
            var type = curObj[0].tagName.toLowerCase();
            if (type == "input" || type == "textarea") {
                curObj.val(msg);
            }
            else if (type == "select") {
                curObj.append("<option id=\"" + key + "_emptyMsg\" selected=\"selected\">" + msg + "</option>");
            }
            curObj.css({ "border-color": "red", "color": "red" });
          
        }

    }
    return result;
}
```

分析`onSubmit`函数，先是会调用`checkData`函数（其代码也在上面）对验证码进行一个初步的空值校验，校验通过后则会声明变量`house`， 其值通过调用`houselist.first({ HouseId: curHouseId })`获取；然后便会发起`ajax`请求，携带的`data`参数就是转换为`json`的`house`值；之后如果响应为`false`则表示验证码验证未通过，否则就会对页面进行渲染，展示出房号详情页面。由此可见，重点是`house`值的获取过程，接下来再进行抓包分析。

![](../images/chapter2/铜仁市/p62.png)

在`house`声明处打上断点进行调试，则发现`houselist`是个`array`对象，包含了整个房号列表页面的所有房号信息。`houselist.first({ HouseId: curHouseId })`则是通过调用`houselist`对象的`first`方法并传入当前房号`id`作为参数，以此来获取房号的详情信息。

![](../images/chapter2/铜仁市/p63.png)

在当前抓包页面（房号列表页）搜索`houselist`，则可发现声明变量`houselist`的地方。因为是在房号列表页面搜索得到，就验证了前面的猜测是正确的，即不需要进入发起详情页请求，且在房号列表页就能获取到所有的房号详情信息。至此，房号详情分析已全部完成。

### 十二、房号详情之数据提取

分析并定位到所有的房号详情信息后，便可进行详情数据提取。需注意的是因为房号详情数据是在 `script`标签即`js`代码中声明的变量值，需先用`re`进行提取到字符串后，再利用`json`模块进行反序列化转换为`python`数据类型后再进行提取。房号详情数据提取的核心代码如下：

```python
def parse_room_li(self, response):
    """
    获取房号列表
    :param response:
    :return:
    """
    
    '''其余代码已省略'''
    
    item_bd = copy(response.meta['item_bd'])
    # 提取房号列表信息
    room_li = self.get_room_li(response.body.decode(), item_bd)
    # 遍历获取房号信息
    for room_dict in room_li:
        item_rm = FdcRoomItem()
        item_rm['projectName'] = item_bd['projectName']
        item_rm['projectId'] = item_bd['projectId']
        item_rm['blockName'] = item_bd['blockName']
        item_rm['buildingId'] = item_bd['buildingId']
        item_rm['roomUse'] = room_dict.get('LayoutTypeName', None)  # 房屋用途
        item_rm['roomStructure'] = room_dict.get('DoorTypeName', None)  # 房屋套型/户型

@staticmethod
def get_room_li(_str: str, item) -> list:
    """
    提取房号列表
    :param _str: 响应字符串
    :param item: 楼栋item
    :return:
    """
    regex = re.compile(r'houselist = eval[(](.*?)[)];', re.S)
    try:
        room_info_str = regex.findall(_str)[0]
        room_li = json.loads(room_info_str, strict=False)
        assert isinstance(room_li, list), '{}-{} 房号信息反序列化后，类型不是list'.format(item['projectName'], item['blockName'])
    except AssertionError as e:
        logger.error(e)
    except Exception as e:
        logger.error('{}-{} 提取房号列表出错，msg：{}'.format(item['projectName'], item['blockName'], e))
        return list()
    else:
        return room_li
```

另外，提取的房号详情信息中并不能直接获取到房号的销售状态，而是通过十六进制的颜色代码进行区分获取，因逻辑较为简单，不需要`js`抓包，直接穷举获取即可。处理销售状态的核心代码如下：

```python
@staticmethod
def get_sale_status(status_str: str):
    """
    获取房号销售状态
    :param status_str:
    :return:
    """
    status_dict = {
        '#8DCB57': '未售',
        '#6D71BD': '签约',
        '#7E2CAC': '备案',
        '#0F83B4': '预告',
        '#B41416': '初登',
        '#B6790F': '转移',
        '#80827F': '限制',
    }
    try:
        assert status_str
    except:
        return
    else:
        for key, value in status_dict.items():
            if key in status_str:
                return value
```

