# 汕头市新房爬虫

### 一、项目目标

1. 爬取网站：[汕头市商品房管理信息平台](http://14.18.103.122/#/)
2. 爬取目标：完成**楼盘 + 楼栋 + 房号**详细信息的爬取、入库、更新

### 二、网站分析

1. **用例图**

![](../images/chapter2/汕头市/p1.png)

2. **项目列表**
>请求`url`: http://14.18.103.122/mp/NewHouse/getProjectList
>
>请求`method`：POST
>
>`Form Data`：其中`OrderInfo`和`pageSize`均为固定值，`PageIndex`为当前页数，其余参数均为空字符串
>
>```json
>{
>	"CompanyXID": "",
>	"HX": "",
>	"ID": "",
>	"ISMAP": "",
>	"LABEL": "",
>	"OrderInfo": "1",
>	"PageIndex": "1",
>	"Region": "",
>	"TotalPrice": "",
>	"XMMC": "",
>	"pageSize": "5"
>}
>```
>
>![](../images/chapter2/汕头市/p2.png)

3. **项目列表翻页**
>请求`url`: http://14.18.103.122/mp/NewHouse/getProjectList
>
>请求`method`：POST
>
>`Form Data`：请求参数中只有`PageIndex`会随着页数变化而变化，其值就是页码数字
>
>```json
>{
>	"CompanyXID": "",
>	"HX": "",
>	"ID": "",
>	"ISMAP": "",
>	"LABEL": "",
>	"OrderInfo": "1",
>	"PageIndex": "4", 
>	"Region": "",
>	"TotalPrice": "",
>	"XMMC": "",
>	"pageSize": "5"
>}
>```
>
>![](../images/chapter2/汕头市/p3.png) 

4. **项目详情**
>请求`url`: http://14.18.103.122/mp/NewHouse/getProjectInfo
>
>请求`method`：POST
>
>`Form Data`：请求参数只有`id`一个，其值为项目`id`
>
>```json
>{
>	"id": "973867"
>}
>```
>
>![](../images/chapter2/汕头市/p4.png)

5. **预售许可证信息**
>请求与**第4条**相同
>
>![](../images/chapter2/汕头市/p5.png)
>
>![](../images/chapter2/汕头市/p6.png)

6. **楼栋信息**
>请求与**第4条**相同
>
>![](../images/chapter2/汕头市/p7.png)
>  
>![](../images/chapter2/汕头市/p8.png)

7. **房号列表**
>请求`url`: http://14.18.103.122/mp/NewHouse/getBuildData
>
>请求`method`：POST
>
>`Form Data`：其中`param`为楼栋相关信息，`fileName`为固定字符串
>
>```json
>{
>	"fileName": "publicbuildtable.config",
>	"param": "{'buildid':'11565','where':[{'grouprid':'129','value':''},{'grouprid':'133','value':''},{'grouprid':'10','value':''},{'grouprid':'862','value':''},{'grouprid':'864','value':''}]}"
>}
>```
>
>![](../images/chapter2/汕头市/p9.png)

8. **房号详情**
>请求`url`: http://14.18.103.122/mp/NewHouse/getRoomInfo
>
>请求`method`：POST
>
>`Form Data`：请求参数只有`FWID`一个，其值为房号`id`
>
>```json
>{
>	"FWID": "100097250",
>}
>```
>
>![](../images/chapter2/汕头市/p10.png)

### 三、存在问题

1. **初步分析、测试，该网站反爬级别较高，只要加上代理`IP`就无法获取数据（堡垒机内代理`IP`直接无法访问，提示访问过于频繁）**
>![](../images/chapter2/汕头市/p14.png)
>
>![](../images/chapter2/汕头市/p11.png)

2. **该网站部分项目会出现不同预售许可证下有相同楼栋的问题，即楼栋存在重复**
>![](../images/chapter2/汕头市/p12.png)
>
>![](../images/chapter2/汕头市/p13.png)
