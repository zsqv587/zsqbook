# 黄石市新房爬虫

### 一、项目目标

1. 爬取网站：[房地产信息发布平台](http://58.51.240.121:8503/)  
2. 爬取目标：完成**楼盘 + 楼栋 + 房号**详细信息的爬取、入库、更新  

### 二、创建项目

```python
# 配置好python虚拟环境
# win+R输入cmd打开黑窗口  

# 创建项目
# 如已有项目则可跳过此步骤
scrapy startproject FDC_spider
```

### 三、创建爬虫

```python
# 切换到项目目录
cd FDC_spider
# 创建爬虫
scrapy genspider huangshi 58.51.240.121
```

### 四、定位爬虫入口

![](../images/chapter2/黄石市/p8.png)

### 五、项目列表首页

![](../images/chapter2/黄石市/p9.png)

点击进入项目列表页，抓包解析可知，请求方式是`GET`，响应内容为`html`字符串，直接用`xpath`进行数据提取即可。核心代码如下：

```python
class HuangshiSpider(scrapy.Spider):
    name = 'huangshi'
    allowed_domains = ['58.51.240.121']
    start_urls = ['http://58.51.240.121:8503/More_xm.aspx']

    def parse(self, response):
        """
        获取项目列表
        :param response:
        :return:
        """
        
        """部分代码省略"""
        
        # 获取当前页数
        page_num = response.meta.get('page_num', None) if response.meta.get('page_num', None) else 1
        try:
            # 获取当前页项目列表分组
            project_tr_li = response.xpath("//b[contains(text(),'项目')]/../../following-sibling::tr")
            assert project_tr_li
        except:
            logger.error(f'第{page_num}页 项目列表获取为空')
        else:
            for project_tr in project_tr_li:
                item_eg = FdcEstateGuidItem()
                item_eg['projectName'] = project_tr.xpath("./td[1]/a/text()").extract_first()  # 项目名称
                item_eg['projectUrl'] = project_tr.xpath("./td[1]/a/@href").extract_first()  # 项目url
                item_eg['projectId'] = self.get_id(item_eg['projectUrl'])  # 项目id
                item_eg['projectAddress'] = project_tr.xpath("./td[2]/text()").extract_first()  # 项目地址
                item_eg['districtName'] = project_tr.xpath("./td[3]/text()").extract_first()  # 项目区域
                item_eg['totalRoomNo'] = project_tr.xpath("./td[4]/text()").extract_first()  # 总套数
                item_eg['saleableRoomNo'] = project_tr.xpath("./td[5]/text()").extract_first()  # 可售套数
```

### 六、项目列表翻页

![](../images/chapter2/黄石市/p3.png)

抓包发现，项目列表翻页与首页的数据结构基本相同，其响应为`html`字符串，请求方式为`GET`，请求`url`从`a`标签超链接中的`href`属性可以直接获取。其中`page`为当前请求的页数，而总页数既可以从**尾页**按钮的`href`属性中获取，也可以从**跳转页面**的`select`下拉选择框中获取。项目列表翻页的核心代码如下：

```python
def parse(self, response):
    """
    获取项目列表
    :param response:
    :return:
    """
	
    """部分代码省略"""
    
    # 获取总页数
    total_page_str = response.xpath(
        "//select[@id='NewProjectList1$AspNetPager1_input']/option[last()]/text()").extract_first()
    total_page_num = self.get_total_page(total_page_str)
    # 翻页
    for next_page_num in range(2, total_page_num + 1):
        yield scrapy.Request(
            self.project_li_temp.format(next_page_num),
            meta=dict(page_num=deepcopy(next_page_num)),
            priority=10,
        )
```

### 七、项目详情

![](../images/chapter2/黄石市/p10.png)

点击项目列表页中的任一项目名，即可进入相应的项目详情页面，如上图所示，**红色方框**为项目详情信息，**蓝色方框**为预售许可证列表信息。经抓包，不难发现为`GET`请求，响应为`html`字符串，请求`url`可从项目列表页中`a`标签超链接中的`href`属性中获取，其参数只有`DevProjectId`在变化，且它的值就是该项目的`projectId`值。

> **红色方框**项目详情信息核心代码如下：

```python
def parse_project_detail(self, response):
    """
    获取项目详情
    :param response:
    :return:
    """
    
    """部分代码省略"""
    
    item_eg = copy(response.meta['item_eg'])
    item_eg['projectUrl'] = response.request.url
    item_eg['developerName'] = response.xpath(
        "//span[@id='ProjectInfo1_lblCorpName']/text()").extract_first()  # 开发商
    item_eg['projectUse'] = response.xpath(
        "//span[@id='ProjectInfo1_lblProjectType']/text()").extract_first()  # 项目用途/项目类型
    item_eg['commencementDate'] = response.xpath(
        "//span[@id='ProjectInfo1_lblJhkgrq']/text()").extract_first()  # 开工日期
    item_eg['coverageArea'] = response.xpath("//span[@id='ProjectInfo1_lblXmzgm']/text()").extract_first()  # 占地面积
    item_eg['completionDate'] = response.xpath(
        "//span[@id='ProjectInfo1_lblJhjfsyrq']/text()").extract_first()  # 竣工日期
```

> **蓝色方框**为预售许可证列表信息核心代码如下：

```python
def parse_project_detail(self, response):
    """
    获取项目详情
    :param response:
    :return:
    """
	
    """部分代码省略"""

    # 获取预售许可证分组
    permit_tr_li = response.xpath("//td[contains(text(),'预售许可')]/../following-sibling::tr")
    if permit_tr_li:
        # 遍历获取预售许可证信息
        for permit_tr in permit_tr_li:
            item_eg['preSalePermit'] = permit_tr.xpath("./td[1]/text()").extract_first()  # 预售许可证
            item_eg['preSaleBuilding'] = permit_tr.xpath("./td[2]/text()").extract_first()  # 预售楼栋
            item_eg['certDate'] = permit_tr.xpath("./td[3]/text()").extract_first()  # 发证日期
            permit_url = permit_tr.xpath("./td[4]/a/@href").extract_first()  # 预售许可证详情url
            item_eg['permitId'] = self.get_id(permit_url)  # 预售许可证id
            yield item_eg
    else:
        logger.warning(
            '{} {} 预售许可证列表获取为空'.format(item_eg['projectUrl'],item_eg['projectName']))
```

### 八、楼栋列表

![](../images/chapter2/黄石市/p11.png)

点击**查看许可证信息**按钮，进入许可证详情页（楼栋列表页），如上图所示，**红色方框**为预售许可证详情信息，**蓝色方框**为楼栋列表信息。抓包发现，请求方式为`GET`，请求`url`可从点击按钮的超链接`href`属性中提取，其参数为`PresellId`即预售许可证`id`，响应为`html`字符串。

> **红色方框**预售许可证详情信息，通过对比发现，其信息在之前已经全部获取，即为重复数据，因此不需要再进行提取。

> **蓝色方框**楼栋列表信息核心代码如下：

```python
def parse_building_li(self, response):
    """
    获取楼栋列表
    :param response:
    :return:
    """
    
    """部分代码省略"""
    
    item_eg = copy(response.meta['item_eg'])
    # 获取楼栋列表分组
    building_tr_li = response.xpath("//td[contains(text(),'总层数')]/../following-sibling::tr")
    if building_tr_li:
        # 遍历获取楼栋信息
        for building_tr in building_tr_li:
            item_bd = FdcBuildingItem()
            item_bd['projectName'] = item_eg['projectName']  # 项目名称
            item_bd['projectId'] = item_eg['projectId']  # 项目id
            item_bd['preSalePermit'] = item_eg['preSalePermit']  # 预售许可证
            item_bd['blockName'] = building_tr.xpath("./td[1]/text()").extract_first()  # 楼栋名字
            item_bd['floorTotalNo'] = building_tr.xpath("./td[2]/text()").extract_first()  # 总层数
            item_bd['totalRoomNo'] = building_tr.xpath("./td[3]/text()").extract_first()  # 总套数
            item_bd['saleableRoomNo'] = building_tr.xpath("./td[4]/text()").extract_first()  # 可售套数
            item_bd['buildingUrl'] = building_tr.xpath("./td[5]/a/@href").extract_first()  # 楼栋url
            item_bd['buildingId'] = self.get_id(item_bd['buildingUrl'])  # 楼栋id
    else:
        logger.warning(
            '{} {}-{} 楼栋列表获取为空'.format(response.request.url, item_eg["projectName"], item_eg['preSalePermit']))	
```

### 九、楼栋详情

![](../images/chapter2/黄石市/p12.png)

点击**查看楼盘表信息**按钮，即可进入楼栋详情页面。经抓包，其请求方式为`GET`，请求`url`可从点击按钮的超链接`href`属性中提取，其参数为`ProjectBuildingID`即楼栋`id`，响应为`html`字符串，包含了楼栋详情信息（如上图）和房号列表信息（其中包含楼栋分区、单元信息，且房号列表信息默认被隐藏虽无法预览，但可以提取）。

> 提取楼栋详情信息核心代码如下：

```python
def parse_building_detail(self, response):
    """
    获取楼栋详情
    :param response:
    :return:
    """
    
    """部分代码省略"""
    
    item_bd = copy(response.meta['item_bd'])
    item_bd['buildingUrl'] = response.request.url
    item_bd['buildingTotalArea'] = response.xpath(
        "//span[@id='BuildingInfo1_lblJzmj']/text()").extract_first()  # 总建筑面积
    item_bd['buildingResidenceArea'] = response.xpath(
        "//span[@id='BuildingInfo1_lblZzmj']/text()").extract_first()  # 楼栋住宅面积
    item_bd['buildingNonResidenceArea'] = response.xpath(
        "//span[@id='BuildingInfo1_lblFzzmj']/text()").extract_first()  # 楼栋非住宅面积
```

### 十、房号列表

> **楼栋分区处理**

![](../images/chapter2/黄石市/p13.png)

经抓包解析，楼栋分区主要分为**未分区**和**分区**两种情况，如上图所示。

**未分区**：逻辑相对简单，所有的房号均在同一分区下，不需要进行区分，直接提取即可。

**分区**：当有分区的时候，存在一个或者多个楼栋的房号按照不同标准进行分区划分的情况，因此需要对所有房号先按照分区进行分组后再进行提取，并添加分区字段，否则会存在重复房号或者漏掉部分房号的情况。

>  **楼栋单元处理**

![](../images/chapter2/黄石市/p14.png)

经抓包解析，楼栋单元存在**无单元**和**有单元**两种情况。

**无单元**：处理相对简单，默认为1单元。

**有单元**：单元信息可能会出现在楼栋分区中（上图左），也可能会出现在房号列表中（上图右）。并且从上图中可以发现，当楼栋分区出现单元信息时，房号列表中也可能会出现疑似单元的相关信息。因此，在提取楼栋单元时，需要优先判断分区中是否有单元信息。如有，即进行单元信息提取（且房号列表中相关信息作废），如没有，再到房号列表中进行提取。

> 分区和楼栋单元处理核心代码如下：

```python
def parse_building_detail(self, response):
    """
    获取楼栋详情
    :param response:
    :return:
    """
	
    """部分代码省略"""

    # 获取分区列表分组
    zone_td_li = response.xpath("//div[@id='BuildingInfo1_tdLPB']/table//td")
    if zone_td_li:
        unit_set = set()
        for zone_td in zone_td_li:
            building_zoning = zone_td.xpath("./b/text()").extract_first()
            # 当分区名称为单元名称
            unit_set.add(building_zoning) if building_zoning and '单元' in building_zoning else False
            # 提取分区id
            zone_td_str = zone_td.xpath("./@onclick").extract_first()
            zone_td_id = self.get_zone_id(zone_td_str)
            if zone_td_id:
                # 获取单元列表分组
                unit_td_li = response.xpath(f"//div[contains(@id,'{zone_td_id}')]//td[@valign='bottom']")
                if unit_td_li:
                    for unit_td in unit_td_li:
                        unit_num = unit_td.xpath("./table/td[2]/text()").extract_first()
                        unit_set.add(unit_num) if unit_num and str(
                            unit_num).strip() and '单元' not in building_zoning else False
                        # 获取房号列表分组
                        room_str_li = unit_td.xpath("./table//a/@onclick").extract()
```

> 处理完分区和楼栋单元后，即可进行房号列表相关信息提取，逻辑相对简单，核心代码如下：

```python
def parse_building_detail(self, response):
    """
    获取楼栋详情
    :param response:
    :return:
    """
	
    """部分代码省略"""
    
    # 获取房号列表分组
    room_str_li = unit_td.xpath("./table//a/@onclick").extract()
    for room_str in room_str_li:
        room_id = self.get_room_id(room_str)
        if room_id:
            # 获取房号详情
            yield scrapy.Request(
                self.room_detail_url.format(room_id),
                callback=self.parse_room_detail,
                priority=50,
                meta=dict(item_bd=deepcopy(item_bd), room_id=deepcopy(room_id),
                          building_zoning=deepcopy(building_zoning), ),
            )
        else:
            logger.error(
                '{}-{}-{} 房号id提取失败'.format(item_bd['projectName'], item_bd['blockName'],room_str))
```

### 十一、房号详情

![](../images/chapter2/黄石市/p7.png)

点击房号列表任一房号，即可进入相应房号的详情页面，如上图。经抓包解析，请求方式为`GET`，请求`url`通过房号列表页的超链接`href`属性即可获取，其参数`xmfwbm`即房号`id`，响应信息为`html`字符串。房号详情核心代码如下：

```python
def parse_room_detail(self, response):
    """
    获取房号详情
    :param response:
    :return:
    """
    
    """部分代码省略"""
    
    item_bd = copy(response.meta['item_bd'])
    item_rm = FdcRoomItem()
    item_rm['roomLocation'] = response.xpath("//span[@id='HouseInfo1_lblZl']/text()").extract_first()
    item_rm['roomFloor'] = response.xpath("//span[@id='HouseInfo1_lblFwlc']/text()").extract_first()
    item_rm['roomNo'] = response.xpath("//span[@id='HouseInfo1_lblFwfh']/text()").extract_first()
    item_rm['roomUse'] = response.xpath("//span[@id='HouseInfo1_lblFwlx']/text()").extract_first()
    item_rm['roomStructure'] = response.xpath("//span[@id='HouseInfo1_lblFwhx']/text()").extract_first()
    item_rm['forecastBuildArea'] = response.xpath("//span[@id='HouseInfo1_lblycfwjzmj']/text()").extract_first()
    item_rm['forecastInnerArea'] = response.xpath("//span[@id='HouseInfo1_lblycfwtnmj']/text()").extract_first()
    item_rm['forecastSharedArea'] = response.xpath("//span[@id='HouseInfo1_lblycfwftmj']/text()").extract_first()
    item_rm['saleStatus'] = response.xpath("//span[@id='HouseInfo1_lblxszt']/text()").extract()
    item_rm['orientation'] = response.xpath("//span[@id='HouseInfo1_lblCx']/text()").extract_first()
    item_rm['recordUnitPrice'] = response.xpath("//span[@id='HouseInfo1_lblnxjg']/text()").extract_first()
    yield item_rm
```

### 十二、字段初步清洗

完成项目（楼盘）、楼栋、房号的全部解析提取后，需要对部分字段的数据在`Pipeline`中进行初步处理后方能入库。其核心代码如下：

```python
class HuangshiPipeline(object):
    """
    黄石市
    """

    def process_item(self, item, spider):
        if spider.name == 'huangshi':
            item['city'] = '黄石市'
            item['sr'] = 'hshfc'
            item['siteName'] = '黄石市房地产市场信息发布平台'
            # 对楼盘/楼栋字段进行初步清洗
            if not isinstance(item, FdcRoomItem):
                for item_eg_tup in item.items():
                    item[item_eg_tup[0]] = self._deal_item_space(item_eg_tup[1])
            # 对房号销售状态字段进行初步清洗
            else:
                item['saleStatus'] = self._deal_sale_status(item['saleStatus'])
        return item
    
    @staticmethod
    def _deal_item_space(_ret):
        regex = re.compile(r'\s{2,}', re.S)
        if isinstance(_ret, str):
            try:
                assert _ret
                ret = regex.sub('', _ret)
                assert ret
            except:
                return
            else:
                return ret
        else:
            return _ret

    @staticmethod
    def _deal_sale_status(_li: list) -> str or None:
        """
        获取销售状态
        :param _li:
        :return:
        """
        try:
            ret = ''.join([str(i).strip() for i in _li if i and str(i).strip()])
            assert ret
        except:
            return
        else:
            return ret
```

