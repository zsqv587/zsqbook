# 重庆市新房爬虫

### 一、项目目标
1. 爬取网站：[重庆网上房地产](http://www.cq315house.com/)
2. 爬取目标：完成**楼盘 + 楼栋 + 房号**详细信息的爬取、入库、更新

### 二、创建项目

```python
# 配置好python虚拟环境
# win+R输入cmd打开黑窗口  

# 创建项目
scrapy startproject FDC_spider
```

### 三、创建爬虫

```python
# 切换到项目目录
cd FDC_spider
# 创建爬虫
scrapy genspider chongqing cq315house.com
```

### 四、定位爬虫入口

![](../images/chapter2/重庆市/p36.png)

### 五、项目列表首页
![](../images/chapter2/重庆市/p37.png)

进入项目列表页，抓包分析后不难发现项目列表信息为`ajax`异步加载。如上图所示，黑色方框为网页项目列表信息，红色圆框为`ajax`请求`url`，蓝色方框为字段对应信息。

```python
{"d":"[{\"projectid\":\"767570\",\"projectname\":\"保亿御景玖园\",\"enterprisename\":\"保亿荣钰重庆房地产开发有限公司\",\"location\":\"北碚区蔡家组团G标准分区G29-1-1/07号宗地\",\"f_presale_cert\":\"渝住建委（2021）预字第（8）号\",\"blockname\":\"6幢（规划6号楼）,5幢（规划5号楼）\",\"buildingid\":\"te6Be8bMPK_jC3sYmWL1nQ,xkhKRhM6ufFT_17YpEZHiw\",\"counts\":\"23976\"},{\"projectid\":\"808923\",\"projectname\":\"麓铭府N17号地块\",\"enterprisename\":\"重庆金碧雅居房地产开发有限公司\",\"location\":\"巴南区玉泉路346号\",\"f_presale_cert\":\"渝住建委（2021）预字第（16）号\",\"blockname\":\"11幢（规划11号楼）\",\"buildingid\":\"Fb5/2EgIsFeFd5j6hY/Dxg\",\"counts\":\"23976\"}...]"}
```

解析列表页响应信息，为`Json`字符串。但其中并没有行政区划字段（可以从location中提取），为省去后续提取行政区划步骤，所以需要分行政区划发起请求。

![](../images/chapter2/重庆市/p38.png)

抓包解析，获取行政区划信息url，核心代码如下：

```python
# 修改start_urls，优先先获取行政区划
start_urls = ['http://www.cq315house.com/HtmlPage/PresaleDetail.html']

def parse(self, response):
    """
    获取行政区列表
    :param response:
    :return:
    """
    # 提取行政区划分组
    option_li = response.xpath("//select[@id='selectxzq']/option[position()>1]")
```
![](../images/chapter2/重庆市/p39.png)

获取行政区划信息后，以渝中区为例，在网页中选中点击查询按钮，获取渝中区首页列表页信息。抓包解析后发现，项目列表url不变，而POST请求新增几个参数。其中siteid最为重要，其值为行政区划对应value值。maxrow和minrow两个参数在切换行政区划获取首页时值不变，分别是11和1。获取项目列表首页信息核心代码如下：

```python
# 修改start_urls，优先先获取行政区划
start_urls = ['http://www.cq315house.com/HtmlPage/PresaleDetail.html']
# POST 项目列表url
project_li_url = 'http://www.cq315house.com/WebService/WebFormService.aspx/getParamDatas2'


def parse(self, response):
    """
    获取行政区列表
    :param response:
    :return:
    """
    # 提取行政区划分组
    option_li = response.xpath("//select[@id='selectxzq']/option[position()>1]")
    minrow, maxrow = (1, 11)
    # 遍历获取行政区划信息
    for option in option_li:
        district_name = option.xpath("./text()").extract_first()
        district_value = option.xpath("./@value").extract_first()
        # 构造请求头
        headers = {
            'Content-Type': 'application/json',
        }
        # 构造请求参数
        data = {
            'areaType': '',
            'entName': '',
            'location': '',
            'maxrow': str(maxrow),
            'minrow': str(minrow),
            'projectname': '',
            'siteid': district_value,
            'useType': '',
        }
        # 构造请求，获取项目列表首页信息
        yield scrapy.Request(
            self.project_li_url,
            method='POST',
            headers=headers,
            body=json.dumps(data),
            callback=self.parse_project_li,
            meta=dict(district_name=district_name, district_value=district_value, page_num=1, ),
            dont_filter=True,
        )
```

### 六、项目列表翻页
![](../images/chapter2/重庆市/p40.png)

进行项目列表页翻页调试后抓包，解析后发现请求url依旧不变，siteid不变，主要是maxrow和minrow两个参数进行联合控制。项目列表翻页核心代码如下：

```python
def parse_project_li(self, response):
    """
    获取项目列表
    :param response:
    :return:
    """
    # 翻页
    # 构造请求头
    next_page_headers = {
        'Content-Type': 'application/json',
        'Referer': self.start_urls[0],
    }
    # 页数+1
    page_num += 1
    # 构造翻页参数
    maxrow = 10 * page_num + 1
    minrow = 10 * (page_num - 1) + 1
    # 构造POST请求参数
    next_page_data = {
        'areaType': '',
        'entName': '',
        'location': '',
        'maxrow': str(maxrow),
        'minrow': str(minrow),
        'projectname': '',
        'siteid': district_value,
        'useType': '',
    }
    # 获取项目列表页信息
    yield scrapy.Request(
        self.project_li_url,
        headers=next_page_headers,
        method='POST',
        body=json.dumps(next_page_data),
        callback=self.parse_project_li,
        meta=dict(district_name=district_name, district_value=district_value, page_num=page_num, ),
        dont_filter=True,
    )
```

实现翻页逻辑后，项目列表页基本完成，且该网站无法获取项目详情页。项目信息提取核心代码如下：

```python
def parse_project_li(self, response):
    """
    获取项目列表
    :param response:
    :return:
    """
    # 行政区划name
    district_name = copy(response.meta['district_name'])
    # 行政区划value
    district_value = copy(response.meta['district_value'])
    # 页数
    page_num = copy(response.meta['page_num'])
    try:
        resp = json.loads(response.body.decode())
        project_li = json.loads(resp['d'])
        assert project_li
    except:
        logger.warning('{}-第{}页 项目列表获取为空'.format(district_name, page_num, ))
    else:
    	# 遍历，获取项目信息
        for project_dict in project_li:
            item_eg = FdcEstateGuidItem()
            item_eg['districtName'] = district_name
            item_eg['projectName'] = project_dict.get('projectname', None)
            item_eg['projectId'] = project_dict.get('projectid', None)
            item_eg['developerName'] = project_dict.get('enterprisename', None)
            item_eg['projectAddress'] = project_dict.get('location', None)
            item_eg['preSalePermit'] = project_dict.get('f_presale_cert', None)
            item_eg['preSaleBlockName'] = project_dict.get('blockname', None)
            yield item_eg
```


### 七、楼栋列表
![](../images/chapter2/重庆市/p41.png)

该网站楼栋信息相对较少，楼栋列表页与项目列表页在同一页面。楼栋列表提取核心代码如下：

```python
# 提取楼栋列表
try:
	# 楼栋id列表
    bd_id_li = project_dict.get('buildingid', None).split(',')
    # 楼栋名列表
    bd_name_li = project_dict.get('blockname', None).split(',')[0:len(bd_id_li)]
    # 判断 楼栋id列表 与 楼栋名列表 长度是否一致，即能否一一对应
    assert len(bd_name_li) == len(bd_id_li)
except:
    logger.error('{}-{}-{} 楼栋名和楼栋id 数量不一致'.format(item_eg['districtName'], item_eg['projectName'],
                                                  item_eg['preSalePermit']))
else:
    # 遍历,获取楼栋信息
    for bd_index, bd_name in enumerate(bd_name_li):
        item_bd = FdcBuildingItem()
        item_bd['districtName'] = item_eg['districtName']
        item_bd['projectName'] = item_eg['projectName']
        item_bd['projectId'] = item_eg['projectId']
        item_bd['preSalePermit'] = item_eg['preSalePermit']
        item_bd['blockName'] = bd_name
        item_bd['buildingId'] = bd_id_li[bd_index]
```

### 八、房号列表
![](../images/chapter2/重庆市/p42.png)

楼栋列表提取完成后，点击楼栋，发现即进入了房号列表页，且抓包分析后发现响应数据仍然由`ajax`异步加载。主要为三个`ajax`请求，响应分别如下：  

> 响应一（红色方框），Json字符串，该响应数据为楼栋详情信息，经对比，与楼栋列表页提取信息全部重复，即不需要再重新获取。

```python
{"d":"{\"enterpriseName\":\"重庆龙湖成恒地产开发有限公司\",\"enterpriseOrgCode\":\"\",\"enterpriseDelegate\":\"\",\"projectName\":\"龙湖时代天街I-3-1-3/06地块\",\"location\":\"渝中区时代天街12号\",\"presaleCert\":\"渝住建委（2020）预字第（1770）号\"}"}
```

> 响应二（蓝色方框），Json字符串，经分析，该响应数据包含销售状态的相关文字信息，且有十六进制的颜色信息，初步猜测与房号列表页的销售状态CSS渲染有关，且存在多条信息，具体调用逻辑暂且未知。

```python
{"d":"[{\"val\":8,\"name\":\"已售\",\"ab\":\"已售\",\"bgColor\":\"#ff00ff\",\"ftColor\":\"#000000\",\"priority\":1,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":64,\"name\":\"不可售\",\"ab\":\"不可售\",\"bgColor\":\"#ffff00\",\"ftColor\":\"#000000\",\"priority\":2,\"type\":0,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":256,\"name\":\"不可售\",\"ab\":\"不可售\",\"bgColor\":\"#ffff00\",\"ftColor\":\"#000000\",\"priority\":3,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":1024,\"name\":\"不可售\",\"ab\":\"不可售\",\"bgColor\":\"#ffff00\",\"ftColor\":\"#000000\",\"priority\":4,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":2048,\"name\":\"已售\",\"ab\":\"已售\",\"bgColor\":\"#ff00ff\",\"ftColor\":\"#000000\",\"priority\":5,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":16777216,\"name\":\"已售\",\"ab\":\"已售\",\"bgColor\":\"#ff00ff\",\"ftColor\":\"#000000\",\"priority\":5,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":131072,\"name\":\"不可售\",\"ab\":\"不可售\",\"bgColor\":\"#ffff00\",\"ftColor\":\"#000000\",\"priority\":6,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":262146,\"name\":\"未售\",\"ab\":\"可售\",\"bgColor\":\"#00ff00\",\"ftColor\":\"#000000\",\"priority\":7,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":262150,\"name\":\"未售\",\"ab\":\"可售\",\"bgColor\":\"#00ff00\",\"ftColor\":\"#000000\",\"priority\":8,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":524292,\"name\":\"未售\",\"ab\":\"可售\",\"bgColor\":\"#00ff00\",\"ftColor\":\"#000000\",\"priority\":10,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":2097152,\"name\":\"已售\",\"ab\":\"已售\",\"bgColor\":\"#ff00ff\",\"ftColor\":\"#000000\",\"priority\":11,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":2621444,\"name\":\"已售\",\"ab\":\"已售\",\"bgColor\":\"#ff00ff\",\"ftColor\":\"#000000\",\"priority\":12,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":20176833,\"name\":\"不可售\",\"ab\":\"不可售\",\"bgColor\":\"#ffff00\",\"ftColor\":\"#000000\",\"priority\":14,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":17301508,\"name\":\"已售\",\"ab\":\"已售\",\"bgColor\":\"#ff00ff\",\"ftColor\":\"#000000\",\"priority\":15,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0},{\"val\":393219,\"name\":\"不可售\",\"ab\":\"不可售\",\"bgColor\":\"#ffff00\",\"ftColor\":\"#000000\",\"priority\":16,\"type\":1,\"alarmType\":1,\"showType\":0,\"parentType\":0,\"treeLevel\":0}]"}
```

> 响应三（黑色方框），Json字符串，不难发现，该响应数据就是房号列表信息，包含大量房号详情相关字段，值得注意的是并没有房号销售状态的相关文字信息，也没有相关的十六进制颜色信息。


```python
{"d":"[{\"id\":1,\"name\":\"1\",\"maxX\":12,\"rooms\":[{\"dataType\":0,\"id\":\"Hw6ydrOniS18p30NjkjOwQ\",\"fjh\":\"YZ31334300010000010100100120001\",\"flr\":\"9\",\"rn\":\"1\",\"y\":12,\"x\":1,\"bArea\":89.22,\"iArea\":72.56,\"vArea\":0,\"sArea\":16.66,\"shareRate\":0,\"uArea\":0,\"prebArea\":0,\"preiArea\":0,\"prevArea\":0,\"presArea\":0,\"preShareRate\":0,\"preuArea\":0,\"stru\":\"钢筋混凝土结构\",\"struId\":0,\"rType\":\"二室二厅\",\"rTypeId\":0,\"landOwnType\":0,\"landKind\":\"\",\"landParcelMemoId\":0,\"useId\":0,\"landUserId\":0,\"use\":\"成套住宅\",\"noSale\":false,\"memo\":null,\"u\":0,\"d\":0,\"l\":0,\"r\":0,\"status\":549758435340,\"stasn\":null,\"stan\":null,\"pid\":0,\"ex1\":null,\"tag\":\"1336567\",\"unitnumber\":\"1\",\"block\":\"1\",\"roomIid\":null,\"location\":\"渝中区时代天街12号1幢9-1\",\"siteId\":0,\"storeyHeight\":0,\"floorUse\":0,\"verandas\":0,\"areaType\":0,\"preSalearea\":0,\"saleArea\":0,\"parcelUsearea\":0,\"parecelSharearea\":0,\"state\":0,\"publicField\":null,\"parcelStartDate\":null,\"parcelEndDate\":null,\"isUnion\":0,\"nsjg\":24046.62,\"nsjmjg\":19556.41,\"sellerId\":0,\"enterpriseId\":0,\"district\":0,\"rightKindId\":0,\"rightTypeId\":0,\"f_zxsp_name\":null,\"zxsp\":0,\"yslx\":null,\"isYjs\":0,\"ysxgsj\":null,\"beizu\":null,\"fsbw\":null,\"F_ISONLINESIGN\":1,\"F_ISCONTRACT\":1,\"F_ISOTHERRIGHT\":0,\"F_ISOWNERSHIP\":0,\"F_ISLIMIT\":0,\"F_ISUSE\":0,\"S_ISONLINESIGN\":null,\"S_ISCONTRACT\":null,\"S_ISOTHERRIGHT\":null,\"S_ISOWNERSHIP\":null,\"S_ISLIMIT\":null,\"S_ISUSE\":null,\"F_PRESALE_CERT\":null,\"F_HOUSE_NO\":\"YZ31334300010000010100100120001\",\"F_FJH\":null,\"F_STATE\":\"期房\",\"F_Business_Status\":null,\"f_nosale_memo\":\"\",\"roomstatus\":\"网签\"},......}
```

将三个ajax请求对应的响应数据分析完成后，找到房号列表页获取入口，即可构造请求获取响应。获取房号列表信息核心代码如下：

```python
def parse_project_li(self, response):
    """
    获取项目列表
    :param response:
    :return:
    """

    '''其余代码已'''

    # 构造请求头
    headers = {
        'Content-Type': 'application/json',
        'Referer': self.building_url_temp.format(item_bd['buildingId'],
 urllib.parse.quote(item_bd['blockName'])),
    }
    # 构造请求参数
    data = {
        'buildingid': item_bd['buildingId'],
    }
    # 获取房号列表
    yield scrapy.Request(
        self.room_li_url,
        method='POST',
        headers=headers,
        body=json.dumps(data),
        callback=self.parse_room_li,
        meta=dict(item_bd=deepcopy(item_bd), ),
        priority=10,
    )

```

完成房号列表页请求构造后，即可进行房号列表信息的提取。需注意跃层的处理、注意部分非规范json字符串的反序列化处理、注意房号需要js的规则构造生成，并在提取房号列表的同时进行楼栋总楼层的获取，并且会发现没有房号销售状态相关信息，这是该网站爬取的几大难点。房号列表提取核心代码如下：

```python
def parse_room_li(self, response):
    """
    获取房号列表
    :param response:
    :return:
    """
    item_bd = copy(response.meta['item_bd'])
    floor_set = set()
    try:
        resp = json.loads(response.body.decode())
        resp_d = self.data_to_json(resp.get('d', None))
        room_li = list()
        for resp_dict in resp_d:
            resp_dict = self.data_to_json(resp_dict)
            resp_room_li = self.data_to_json(resp_dict.get('rooms', None))
            room_li += resp_room_li
        assert room_li
    except Exception as e:
        logger.error(
            '{}-{}-{}-{} 房号列表获取出错，error_msg:{}'.format(item_bd['districtName'], item_bd['projectName'],
                                                       item_bd['preSalePermit'], item_bd['blockName'], e))
    else:
        for room_dict in room_li:
            rn = room_dict.get('rn', None)
            if '跃' not in rn or '跃1' in rn:  # 跃层处理
                item_rm = FdcRoomItem()
                item_rm['projectName'] = item_bd['projectName']
                item_rm['projectId'] = item_bd['projectId']
                item_rm['preSalePermit'] = item_bd['preSalePermit']
                item_rm['blockName'] = item_bd['blockName']
                item_rm['buildingId'] = item_bd['buildingId']
                item_rm['roomId'] = room_dict.get('id', None)
                item_rm['roomRegistrationNum'] = room_dict.get('fjh', None)
                item_rm['physicsFloor'] = room_dict.get('y', None)
                item_rm['nominalFloor'] = room_dict.get('flr', None)
                floor_set.add(item_rm['nominalFloor'])  # 用于提取楼栋总楼层
                item_rm['roomNo'] = self._get_room_no(item_rm['nominalFloor'], rn)
                item_rm['buildingArea'] = room_dict.get('bArea', None)
                item_rm['innerArea'] = room_dict.get('iArea', None)
                item_rm['apportionmentArea'] = room_dict.get('sArea', None)
                item_rm['roomLocation'] = room_dict.get('location', None)
                item_rm['unitNo'] = room_dict.get('unitnumber', None)
                item_rm['roomUse'] = room_dict.get('use', None)
                item_rm['roomStructure'] = room_dict.get('rType', None)
                item_rm['buildingStructure'] = room_dict.get('stru', None)
                item_rm['buildingAreaUnitPrice'] = room_dict.get('nsjmjg', None)
                item_rm['innerAreaUnitPrice'] = room_dict.get('nsjg', None)
    finally:
        # 提取楼栋总楼层
        try:
            assert floor_set
            floor_li = [int(i) for i in list(floor_set) if '负' not in i]
            assert floor_li
            floor_li.sort(reverse=True)
        except:
            item_bd['floorTotalNo'] = None
        else:
            item_bd['floorTotalNo'] = floor_li[0]
        finally:
            yield item_bd
```

### 九、房号详情：验证码抓包
![](../images/chapter2/重庆市/p43.png)

完成楼栋信息提取后，点击房号，进入房号详情之前，会弹出验证码，take it easy。继续抓包解析后，就能轻松获取到验证码接口。如上图所示。

![](../images/chapter2/重庆市/p44.png)

输入验证码，继续进行抓包，同样能轻松获取到校验接口，并且可以发现已经能获取房号详情数据。值得注意的是房号详情为图片，而且仍然没有销售状态相关字段。如上图所示。

### 十、房号详情：`JS`调试

![](../images/chapter2/重庆市/p46.png)

前面已经抓包获取到验证码校验接口，关键字为`checkcode`，在Chrome开发者工具中利用`Search`输入并搜索`checkcode`关键字，定位得到`validateCode.js`文件。如上图所示。

![](../images/chapter2/重庆市/p47.png)

点击进入`validateCode.js`文件，继续搜索`checkcode`关键字，定位得到`valiteCode`函数，其完整代码如下：

```javascript
function valiteCode() {
    var inputCode = $("#wqvalcode").val();
    if (!inputCode) {
        alert("验证码不能为空");
    } else {
        $.ajax({
            async: false,
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            url: RouteData.serviceUrl2 + "/CheckCode",
            data: "{\"code\":\"" + inputCode + "\"}",
            error: function (obj) {
                changeCode();
            },
            success: function (obj) {
                var jsonData = JSON.parse(obj.d);
                if (jsonData == "1") {
                    var roomId = common.getParam("fid");
                    bindRoomData();
                    $("#valiteCode").css("display", "none"); 
                    $("#DivInfo").css("display", "block");
                    //$("#valiteCode").hide();
                    //$("#DivInfo").show();
                }
                else {
                    alert("验证码错误！");
                    $("#wqvalcode").val("");
                    changeCode();
                }
            }
        });
    }
}
```

对`valiteCode`函数进行代码解析，不难发现该函数在做完验证码空值校验后会发起一个`ajax`请求。`ajax`请求如果响应出错会调用`changeCode`函数更换验证码；而如果响应成功，会继续对响应数据进行校验。如果校验通过，则会获取当前房号id， 再调用`bindRoomData`函数，之后对页面`CSS`样式进行更改；如果检验未通过，会调用`changeCode`函数更换验证码。显然，`bindRoomData`函数是我们需要解析的重点，需对该函数获取后进行分析。

![](../images/chapter2/重庆市/p48.png)

在`bindRoomData`函数处打上断点，进行页面调试，便可获取到该函数位置，其完成代码如下：

```javascript
function bindRoomData() {
    var fid = common.getParam("fid");
    //var result = getRoomDatas(fid);
    try {
    //    var obj = {};
    //    obj["FH"] = result["flr"] + "-" + result["x"];
    //    obj["LH"] = result["block"];
    //    obj["TNMJ"] = result["iArea"];
    //    obj["JZMJ"] = result["bArea"];
    //    obj["SYYT"] = result["use"];
    //    obj["HX"] = result["rType"];
    //    obj["NSDJ_TN"] = result["nsjg"];
    //    obj["NSDJ_JM"] = result["nsjmjg"];
    //    obj["YSXKZ"] = result["F_PRESALE_CERT"];
    //    obj["JZJG"] = result["stru"];
    //    obj["SCZTXX"] = result["roomstatus"];
    //    obj["TDXZ"] = result["landKind"];
    //    var text = JSON.stringify(obj);
    //    text = escape(text);
        $("#roomInfo_img").attr("src", "../WebService/Handler.ashx?ac=fwxx1&fid=" + fid);
    }
    catch (e) {
    }
}
```

解析`bindRoomData`函数代码，发现大量代码被注释，实际是因为该网站验证码校验更新过，注释的代码为之前的校验逻辑。整个函数在获取`fid`（实际就是房号id）后，更改id为`roomInfo_img` 的`img`标签的`src`属性，也就是获取房号详情的url地址，其构造规则为`"../WebService/Handler.ashx?ac=fwxx1&fid="`加上房号id。之后便可抓包获取房号详情。

![](../images/chapter2/重庆市/p45.png)

以上便是整个房号详情的整个`JS`解析流程，是不是挺easy的，但仍有两个问题：  
>问题一：验证码与房号详情都是图片，该如何解决？  
>
>>回答：通常图片的解决方式都是进行图像识别或者调用打码平台付费接口来解决，但该网站有些特殊。首先说图片验证码，该网站的验证逻辑有一个漏洞，通过前面的`JS`解析可以发现整个验证逻辑最后呈现给我们的就是更改`img`标签的`src`属性，而其构造规则前面已经固定，变量只有房号id，而房号id在前面房号列表页已经可以获取到，言外之意就是可以在`python`程序中进行url构造后直接绕过验证码。然后是房号详情，就是一张图片，上面的房号信息也固定，而且有用信息在房号列表页也能全部拿到，言外之意就是完全没必要再请求一次房号详情页。该问题就不再是问题了。  

>问题二：至始至终都没拿到房号销售状态的直接信息，该如何解决？  
>
>>回答：  其实在前面<font color=red>【八、房号列表】</font>步骤中的响应二（蓝色方框）中就已经提到该响应可能与房号销售状态相关，以此为线索，继续进行`JS`调试解析即可获得。大家可尝试自己独立进行解决。

