# 汕头市新房爬虫

### 一、项目目标

1. 爬取网站：[汕头市商品房管理信息平台](http://14.18.103.122/#/)
2. 爬取目标：完成**楼盘 + 楼栋 + 房号**详细信息的爬取、入库、更新

### 二、创建项目

```python
# 配置好python虚拟环境
# win+R输入cmd打开黑窗口  

# 创建项目
# 如已有项目则可跳过此步骤
scrapy startproject FDC_spider
```

### 三、创建爬虫

```python
# 切换到项目目录
cd FDC_spider
# 创建爬虫
scrapy genspider shantou 14.18.103.122
```

### 四、定位爬虫入口

![](../images/chapter2/汕头市/p15.png)

### 五、项目列表首页

> **加代理访问网站**

![](../images/chapter2/汕头市/p11.png)

> **不加代理访问网站**

![](../images/chapter2/汕头市/p14.png)

经测试，发现**添加VPS代理**后，浏览器打开网站但无法获取数据（`POSTMAN`、`Selenium`、`requests`测试均是无法获取数据），而不添加代理，即可正常获取数据，如上面两图所示。所以推测现有的VPS代理无法用于该网站爬取，考虑到时间和资源成本，该网站不使用代理进行开发和爬取。

![](../images/chapter2/汕头市/p2.png)

如上图，进入项目列表页，抓包解析可知，请求方式是`POST`，请求`url`见代码，响应内容为`json`字典，请求体参数中`OrderInfo`和`pageSize`均为固定值，`PageIndex`为当前页数，其余参数均为空字符串。核心代码如下：

```python
"""部分代码省略"""

class ShantouSpider(scrapy.Spider):
    name = 'shantou'
    allowed_domains = ['14.18.103.122']
    start_urls = ['http://14.18.103.122/']
    project_li_url = 'http://14.18.103.122/mp/NewHouse/getProjectList'  # POST 项目列表页

    def start_requests(self):
        """
        项目列表首页
        :return:
        """
        data: dict = {
            "CompanyXID": "",
            "HX": "",
            "ID": "",
            "ISMAP": "",
            "LABEL": "",
            "OrderInfo": "1",
            "PageIndex": "1",
            "Region": "",
            "TotalPrice": "",
            "XMMC": "",
            "pageSize": "5"
        }
        yield scrapy.FormRequest(
            self.project_li_url,
            method='POST',
            headers=self.get_headers(),
            formdata=data,
            meta=dict(page_num=1, ),
        )

    def parse(self, response):
        """
        项目列表页
        :param response:
        :return:
        """
        page_num = copy(response.meta['page_num'])
        try:
            resp_dict = json.loads(response.body.decode())
            project_li = resp_dict['Data'].get('DataList', list())
            assert project_li, f'第{page_num}页，项目列表获取为空'
        except AssertionError as e:
            logger.error(e)
        except Exception as e:
            logger.error(f'第{page_num}页，项目列表获取出错，msg:{e if e else "error"}')
        else:
            # 遍历，提取项目列表信息
            for project_dict in project_li:
                item_eg = FdcEstateGuidItem()
                item_eg['projectName'] = project_dict.get('XMMC', None)  # 项目名称
                item_eg['projectId'] = project_dict.get('XID', None)  # 项目id
                item_eg['planningUse'] = project_dict.get('GHYT', None)  # 规划用途
                item_eg['districtName'] = project_dict.get('SZQY', None)  # 项目区域
                item_eg['projectAddress'] = project_dict.get('JYDZ', None)  # 项目地址
```

### 六、项目列表翻页

![](../images/chapter2/汕头市/p3.png)

抓包发现，项目列表翻页与首页的请求构造基本相同，其响应也是`json`字典，请求体参数中只有`PageIndex`会随着页数变化而变化，其值就是页码数字。项目列表翻页的核心代码如下：

```python
"""部分代码省略"""

def parse(self, response):
    """
    项目列表页
    :param response:
    :return:
    """
    page_num = copy(response.meta['page_num'])
    try:
        resp_dict = json.loads(response.body.decode())
        total_page_num = int(resp_dict['Data'].get('PageCount', None))
        assert total_page_num, f'第{page_num}页，总页数获取出错'
    except AssertionError as e:
        logger.error(e)
    except Exception as e:
        logger.error(f'第{page_num}页，项目列表获取出错，msg:{e if e else "error"}')
    else:
        # 翻页
        data: dict = {
            "CompanyXID": "",
            "HX": "",
            "ID": "",
            "ISMAP": "",
            "LABEL": "",
            "OrderInfo": "1",
            "Region": "",
            "TotalPrice": "",
            "XMMC": "",
            "pageSize": "5"
        }
        for next_page_num in range(2, total_page_num + 1):
            data['PageIndex'] = str(next_page_num)
            yield scrapy.FormRequest(
                self.project_li_url,
                method='POST',
                headers=self.get_headers(),
                formdata=data,
                meta=dict(page_num=next_page_num, ),
                priority=10,
            )
```

### 七、项目详情

![](../images/chapter2/汕头市/p4.png)

点击项目列表页中的任一项目名，即可进入相应的项目详情页面，如上图所示。请求方式为`POST`，请求`url`见代码，响应为`json`字典，请求体参数只有`id`一个，其值为项目`id`。核心代码如下：

```python
"""部分代码省略"""

class ShantouSpider(scrapy.Spider):
    name = 'shantou'
    allowed_domains = ['14.18.103.122']
    start_urls = ['http://14.18.103.122/']
    project_li_url = 'http://14.18.103.122/mp/NewHouse/getProjectList'  # POST 项目列表页
    project_detail_url = 'http://14.18.103.122/mp/NewHouse/getProjectInfo'  # POST 项目详情页
    
    def parse(self, response):
        """
        项目列表页
        :param response:
        :return:
        """
        page_num = copy(response.meta['page_num'])
        try:
            resp_dict = json.loads(response.body.decode())
            project_li = resp_dict['Data'].get('DataList', list())
            assert project_li, f'第{page_num}页，项目列表获取为空'
        except AssertionError as e:
            logger.error(e)
        except Exception as e:
            logger.error(f'第{page_num}页，项目列表获取出错，msg:{e if e else "error"}')
        else:
            # 遍历，提取项目列表信息
            for project_dict in project_li:
                item_eg = FdcEstateGuidItem()
                item_eg['projectName'] = project_dict.get('XMMC', None)  # 项目名称
                item_eg['projectId'] = project_dict.get('XID', None)  # 项目id

                # 获取项目详情
                if item_eg['projectId']:
                    data: dict = {
                        'id': item_eg['projectId'],
                    }
                    yield scrapy.FormRequest(
                        self.project_detail_url,
                        headers=self.get_headers(),
                        formdata=data,
                        callback=self.parse_project_detail,
                        meta=dict(item_eg=deepcopy(item_eg), ),
                        priority=20,
                    )
                else:
                    logger.error(f"第{page_num}页-{item_eg['projectName']}，项目id获取为空，无法获取项目详情")

    def parse_project_detail(self, response):
        """
        项目详情
        :param response:
        :return:
        """
        item_eg = copy(response.meta['item_eg'])
        try:
            data_dict: dict = json.loads(response.body.decode()).get('Data', dict())
            assert data_dict, f'{item_eg["projectName"]} 项目详情信息获取为空'
            base_info: dict = data_dict.get('BaseInfo', dict())
        except AssertionError as e:
            logger.error(e)
        except:
            logger.error(f'{item_eg["projectName"]} 项目详情信息获取出错')
        else:
            item_eg['estateTotalArea'] = base_info.get('JZMJ', None)  # 建筑总面积
            item_eg['floorAreaRatio'] = base_info.get('RJL', None)  # 容积率
            item_eg['greeningRate'] = base_info.get('LDV', None)  # 绿化率
            item_eg['latBd'] = base_info.get('LATITUDE', None)  # 经度
            item_eg['lngBd'] = base_info.get('LONGITUDE', None)  # 纬度
```

### 八、预售许可证信息

> **预售许可证列表信息**

![](../images/chapter2/汕头市/p5.png)

> **预售许可证详情信息**

![](../images/chapter2/汕头市/p6.png)

如上两图所示，抓包发现，该网站预售许可证信息与项目详情信息在同一个响应中，即不需要再另外构造请求，直接从**七、项目详情**的响应中提取即可。提取的核心代码如下：

```python
"""部分代码省略"""

def parse_project_detail(self, response):
    """
    项目详情
    :param response:
    :return:
    """
    item_eg = copy(response.meta['item_eg'])
    try:
        data_dict: dict = json.loads(response.body.decode()).get('Data', dict())
        assert data_dict, f'{item_eg["projectName"]} 项目详情信息获取为空'
        permit_li: list = data_dict.get('XKZ', list())
    except AssertionError as e:
        logger.error(e)
    except:
        logger.error(f'{item_eg["projectName"]} 项目详情信息获取出错')
    else:
        if permit_li:
            # 遍历，获取预售许可证信息
            for permit_dict in permit_li:
                item_eg['preSalePermit'] = permit_dict.get('XKZH', None)  # 预售许可证
                item_eg['certDate'] = permit_dict.get('FZRQ', None)  # 发证日期
                item_eg['certStartDate'] = permit_dict.get('YXQX1', None)  # 有效期起
                item_eg['certExpiryDate'] = permit_dict.get('YXQX2', None)  # 有效期止
                item_eg['approvalRoomNo'] = permit_dict.get('YSZTS', None)  # 批准套数
                item_eg['approvalArea'] = permit_dict.get('YSZMJ', None)  # 批准面积
                item_eg['preSaleBuilding'] = permit_dict.get('BindBuild', None)  # 预售楼栋
                yield item_eg
```

### 九、楼栋信息

> **楼栋列表信息**

![](../images/chapter2/汕头市/p7.png)

> **楼栋详情信息**

![](../images/chapter2/汕头市/p8.png)

如上两图所示，抓包发现，楼栋信息仍与项目详情信息在同一个响应中，不需要构造请求，直接在`json`响应中提取即可。不过，需要注意的是**该网站部分项目会出现不同预售许可证下有相同楼栋的问题（即楼栋存在重复）**，如下两图所示：

> **汕濠房现字（2021）第001号--10幢**

![](../images/chapter2/汕头市/p12.png)

> **汕濠房现字（2019）第032号--10幢**

![](../images/chapter2/汕头市/p13.png)

可以发现，许可证号**汕濠房现字（2021）第001号**和**汕濠房现字（2019）第032号**下都有**10幢**，且楼栋基本信息完全相同。因此，提取楼栋信息时，遍历提取楼栋信息，将单个楼栋存入一个字典（楼栋`id`作为`key`，楼栋信息作为`value`）；然后将所有楼栋存入一个列表中，对于列表中不存在的楼栋新增，对于存在的楼栋就进行相应字段修改。提取的核心代码如下：

```python
"""部分代码省略"""

def parse_project_detail(self, response):
    """
    项目详情
    :param response:
    :return:
    """
    item_eg = copy(response.meta['item_eg'])
    try:
        data_dict: dict = json.loads(response.body.decode()).get('Data', dict())
        permit_li: list = data_dict.get('XKZ', list())
    except AssertionError as e:
        logger.error(e)
    except:
        logger.error(f'{item_eg["projectName"]} 项目详情信息获取出错')
    else:
        if permit_li:
            build_filter_dict = dict()  # 用于重复楼栋的处理
            # 遍历，获取预售许可证信息
            for permit_dict in permit_li:
                build_li: list = permit_dict.get('BUILDLIST', list())
                if build_li:
                    # 遍历，提取楼栋列表信息
                    for build_dict in build_li:
                        build_id = build_dict['BLDID']
                        bd_dict = build_filter_dict.get(build_id, dict())
                        bd_dict['projectName'] = item_eg['projectName']  # 项目名称
                        bd_dict['projectId'] = item_eg['projectId']  # 项目id
                        bd_dict['buildingId'] = build_id  # 楼栋id
                        bd_dict['blockName'] = build_dict.get('ZMC', None)  # 楼栋名字
                        bd_dict['handoverDate'] = build_dict.get('JFRQ', None)  # 交房日期
                        bd_dict['floorTotalNo'] = build_dict.get('ZCS', None)  # 总层数
                        build_filter_dict[build_id] = bd_dict
                else:
                    logger.warning('{}-{} 楼栋列表获取为空'.format(item_eg['projectName'], item_eg['preSalePermit']))

            # 遍历，实例化楼栋item
            for bd_id, bd_dict in build_filter_dict.items():
                item_bd = FdcBuildingItem()
                for key, value in bd_dict.items():
                    item_bd[key] = value
```

### 十、房号列表

> **房号列表抓包**

![](../images/chapter2/汕头市/p9.png)

请求`url`: http://14.18.103.122/mp/NewHouse/getBuildData

请求`method`：POST

`Form Data`：利用Chrome开发者工具抓包调试，获取请求体参数如下，并可以大致推测参数中`param`为楼栋相关信息，`fileName`为固定字符串 

```json
{
	"fileName": "publicbuildtable.config",
	"param": "{'buildid':'11565','where':[{'grouprid':'129','value':''},{'grouprid':'133','value':''},{'grouprid':'10','value':''},{'grouprid':'862','value':''},{'grouprid':'864','value':''}]}"
}
```


> **请求体参数解析**

![](../images/chapter2/汕头市/p16.png)

对请求体参数进行抓包解析，首先选取请求`url`中`getBuildData`作为关键字在开发者工具`Search`中搜索，便可定位到响应的`js`文件；然后点击进入`js`，继续搜索`getBuildData`关键词，就定位到了`getBulidingData`函数；接着打上断点进行调试，发现该函数实际是发起一个`POST`请求，请求`url`就是房号列表的`url`，而传入的参数`e`通过堆栈可以发现它是一个`js`对象，且可以发现该对象拥有的`fileName`和`param`属性也正好就是请求体参数所需要的参数值。因此，`getBulidingData`函数便是接下来需要分析的重点。

![](../images/chapter2/汕头市/p17.png)

同理，以`getBulidingData`作为关键字进行搜索，便可定位到`choiceBuild`函数，函数调用`getBulidingData`时传入的参数`t`，通过堆栈可以发现其值就是当前请求的楼栋`id`，`choiceBuild`函数详细代码如下：

```javascript
choiceBuild: function(t, a) {
                    console.log(t),
                    console.log(a),
                    this.CFMS = a,
                    this.buildid = t,
                    this.choiceSelectData.FWYT = "",
                    this.choiceSelectData.HX = "",
                    this.choiceSelectData.ZT = "",
                    this.choiceSelectData.JZMJ = "",
                    this.choiceSelectData.TNMJ = "",
                    this.getBulidingData(t)
                }
```

继续查看，便可获取`getBulidingData`函数的核心代码如下：

```javascript
 getBulidingData: function(t) {
                    var a = this;
                    console.log(this.selectData),
                    console.log(this.choiceSelectData);
                    var e = {
                        buildid: t,
                        where: []
                    };
                    e.where.push({
                        grouprid: this.selectData.UsingOptions.grouprid,
                        value: this.choiceSelectData.FWYT
                    }),
                    e.where.push({
                        grouprid: this.selectData.houseTypeOptions.grouprid,
                        value: this.choiceSelectData.HX
                    }),
                    e.where.push({
                        grouprid: this.selectData.statusOptions.grouprid,
                        value: this.choiceSelectData.ZT
                    }),
                    e.where.push({
                        grouprid: this.selectData.areaOptions.grouprid,
                        value: this.choiceSelectData.JZMJ
                    }),
                    e.where.push({
                        grouprid: this.selectData.insideAreaOptions.grouprid,
                        value: this.choiceSelectData.TNMJ
                    }),
                    console.log("筛选条件", e),
                    this.$api.getBulidingData({
                        param: JSON.stringify(e),
                        fileName: "publicbuildtable.config"
                    }).then((function(t) {
                        console.log("楼盘表数据", t),
                        ......
```

解析`getBulidingData`函数，发现会先声明对象`e`，该对象拥有`buildid`属性其值为`t`（也就是楼栋`id`），而`where`属性其值为一个空的`array`；然后会通过`e.where.push`向`array`中添加值，一共会添加五次，每一次都会添加具有`grouprid`和`value`属性的对象，`value`属性的值可以通过`choiceBuild`函数 发现全部都是空字符串，而`grouprid`属性的解析重点就是`selectData`。

![](../images/chapter2/汕头市/p19.png)

通过断点调试和堆栈追踪，可以发现`selectData`对象主要拥有五个属性，其值如上图所示。而`selectData`对象的属性是如何生成确定的，带着这样的疑问，重新回到项目详情页进行抓包，便可发现关键字为`GetPublicDicList`的请求返回的响应，正好就是需要的`selectData`，如下图：

![](../images/chapter2/汕头市/p18.png)

不难发现，`selectData`对象主要拥有的五个属性，其实就是就发起`POST`请求时对应的五个可选项，分别是面积、户型、用途、状态、套内面积，而每个可选项都有一个`grouprid`，其值固定。可选项如下图：

![](../images/chapter2/汕头市/p20.png)

综上，便可对每一次`e.where.push`的添加操作设置对应值，五次添加操作后便完成对象`e`的赋值，然后会调用`getBulidingData`函数，传入的参数包含两个属性，分别就是`param`和`fileName`。其中`param`的值就是通过`JSON.stringify(e)`将对象`e`转化为`json`字符串，`fileName`的值则为固定的`publicbuildtable.config`字符串。

至此，便完成了整个房号列表请求体参数解析。房号列表请求构造的核心代码如下：

```python
"""部分代码省略"""

# 构造POST请求，获取楼栋详情(房号列表)
data: dict = {
    'param': {"buildid": bd_id,
              "where": [{"grouprid": "129", "value": ""}, {"grouprid": "133", "value": ""},
                        {"grouprid": "10", "value": ""}, {"grouprid": "862", "value": ""},
                        {"grouprid": "864", "value": ""}]},
    'fileName': 'publicbuildtable.config',
}
data['param'] = json.dumps(data['param'])
yield scrapy.FormRequest(
    self.building_detail_url,
    headers=self.get_headers(),
    formdata=data,
    callback=self.parse_building_detail,
    meta=dict(item_bd=deepcopy(item_bd), ),
    priority=30,
)
```

### 十一、房号详情

![](../images/chapter2/汕头市/p10.png)

通过抓包，不难发现房号详情的请求解析如下：

请求`url`: http://14.18.103.122/mp/NewHouse/getRoomInfo

请求`method`：POST

`Form Data`：请求参数只有`FWID`一个，其值为房号`id`

房号详情的核心代码如下：

```python
"""部分代码省略"""

# 构造POST请求，获取房号详情
data: dict = {
    'FWID': item_rm['roomId']
}
yield scrapy.FormRequest(
    self.room_detail_url,
    headers=self.get_headers(),
    formdata=data,
    callback=self.parse_room_detail,
    meta=dict(item_rm=deepcopy(item_rm), ),
    priority=40,
)
```

